
## 3.2.4 [10-15-2024]

* Changes made at 2024.10.14_19:56PM

See merge request itentialopensource/adapters/adapter-openstack_neutron!17

---

## 3.2.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-openstack_neutron!15

---

## 3.2.2 [08-14-2024]

* Changes made at 2024.08.14_18:04PM

See merge request itentialopensource/adapters/adapter-openstack_neutron!14

---

## 3.2.1 [08-07-2024]

* Changes made at 2024.08.06_19:17PM

See merge request itentialopensource/adapters/adapter-openstack_neutron!13

---

## 3.2.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!12

---

## 3.1.4 [03-28-2024]

* Changes made at 2024.03.28_13:11PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!11

---

## 3.1.3 [03-21-2024]

* Changes made at 2024.03.21_13:49PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!10

---

## 3.1.2 [03-11-2024]

* Changes made at 2024.03.11_15:29PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!9

---

## 3.1.1 [02-28-2024]

* Changes made at 2024.02.28_11:40AM

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!8

---

## 3.1.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!7

---

## 3.0.3 [12-11-2023]

* Update healthcheck endpoint

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!6

---

## 3.0.2 [01-11-2023]

* Update healthcheck endpoint

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!6

---

## 3.0.1 [01-09-2023]

* Patch/adapt 2521

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!5

---

## 3.0.0 [12-02-2022]

* ADAPT-2415

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!3

---

## 2.0.1 [12-02-2022]

* Update AUTH.md for ISD-3993

Closes #1

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!4

---

## 2.0.0 [11-29-2022]

* Revert the bug in all paths

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!2

---

## 1.0.0 [11-01-2022]

* Major/adapt 2415

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!1

---

## 0.1.1 [09-05-2022]

* Bug fixes and performance improvements

See commit 35ca066

---
