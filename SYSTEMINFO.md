# Openstack Neutron

Vendor: Openstack
Homepage: https://www.openstack.org/

Product: Neutron
Product Page: https://docs.openstack.org/neutron/latest/

## Introduction
We classify OpenStack Neutron into the Cloud domain as Neutron provides users with flexible and scalable virtual networking capabilities for managing network connectivity within cloud environments.


## Why Integrate
The OpenStack Neutron adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Neutron. With this adapter you have the ability to perform operations on items such as:

- Networks
- QoS
- Ports
- Policies


## Additional Product Documentation
The [API documents for OpenStack Neutron](https://docs.openstack.org/api-ref/network/v2/index.html)