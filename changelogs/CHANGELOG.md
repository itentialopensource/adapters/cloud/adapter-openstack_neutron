
## 3.0.2 [01-11-2023]

* Update healthcheck endpoint

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!6

---

## 3.0.1 [01-09-2023]

* Patch/adapt 2521

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!5

---

## 3.0.0 [12-02-2022]

* ADAPT-2415

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!3

---

## 2.0.1 [12-02-2022]

* Update AUTH.md for ISD-3993

Closes #1

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!4

---

## 2.0.0 [11-29-2022]

* Revert the bug in all paths

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!2

---

## 1.0.0 [11-01-2022]

* Major/adapt 2415

See merge request itentialopensource/adapters/cloud/adapter-openstack_neutron!1

---

## 0.1.1 [09-05-2022]

* Bug fixes and performance improvements

See commit 35ca066

---
