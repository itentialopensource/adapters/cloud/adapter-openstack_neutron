/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-openstack_neutron',
      type: 'OpenstackNeutron',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const OpenstackNeutron = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Openstack_neutron Adapter Test', () => {
  describe('OpenstackNeutron Class Tests', () => {
    const a = new OpenstackNeutron(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('openstack_neutron'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('openstack_neutron'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('OpenstackNeutron', pronghornDotJson.export);
          assert.equal('Openstack_neutron', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-openstack_neutron', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('openstack_neutron'));
          assert.equal('OpenstackNeutron', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-openstack_neutron', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-openstack_neutron', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#listAPIVersions - errors', () => {
      it('should have a listAPIVersions function', (done) => {
        try {
          assert.equal(true, typeof a.listAPIVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAPIV2Details - errors', () => {
      it('should have a showAPIV2Details function', (done) => {
        try {
          assert.equal(true, typeof a.showAPIV2Details === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listExtensions - errors', () => {
      it('should have a listExtensions function', (done) => {
        try {
          assert.equal(true, typeof a.listExtensions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showExtensionDetails - errors', () => {
      it('should have a showExtensionDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showExtensionDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alias', (done) => {
        try {
          a.showExtensionDetails(null, (data, error) => {
            try {
              const displayE = 'alias is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showExtensionDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showNetworkDetails - errors', () => {
      it('should have a showNetworkDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showNetworkDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.showNetworkDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showNetworkDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetwork - errors', () => {
      it('should have a updateNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetwork - errors', () => {
      it('should have a deleteNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworks - errors', () => {
      it('should have a listNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetwork - errors', () => {
      it('should have a createNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showNetworkSegmentRangeDetails - errors', () => {
      it('should have a showNetworkSegmentRangeDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showNetworkSegmentRangeDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSegmentRangeId', (done) => {
        try {
          a.showNetworkSegmentRangeDetails(null, (data, error) => {
            try {
              const displayE = 'networkSegmentRangeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showNetworkSegmentRangeDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSegmentRange - errors', () => {
      it('should have a updateNetworkSegmentRange function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSegmentRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSegmentRangeId', (done) => {
        try {
          a.updateNetworkSegmentRange('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkSegmentRangeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateNetworkSegmentRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSegmentRange - errors', () => {
      it('should have a deleteNetworkSegmentRange function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSegmentRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkSegmentRangeId', (done) => {
        try {
          a.deleteNetworkSegmentRange(null, (data, error) => {
            try {
              const displayE = 'networkSegmentRangeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteNetworkSegmentRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkSegmentRanges - errors', () => {
      it('should have a listNetworkSegmentRanges function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkSegmentRanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSegmentRange - errors', () => {
      it('should have a createNetworkSegmentRange function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSegmentRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showPortDetails - errors', () => {
      it('should have a showPortDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showPortDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portId', (done) => {
        try {
          a.showPortDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showPortDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePort - errors', () => {
      it('should have a updatePort function', (done) => {
        try {
          assert.equal(true, typeof a.updatePort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portId', (done) => {
        try {
          a.updatePort(null, null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updatePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePort - errors', () => {
      it('should have a deletePort function', (done) => {
        try {
          assert.equal(true, typeof a.deletePort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portId', (done) => {
        try {
          a.deletePort(null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deletePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPorts - errors', () => {
      it('should have a listPorts function', (done) => {
        try {
          assert.equal(true, typeof a.listPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPort - errors', () => {
      it('should have a createPort function', (done) => {
        try {
          assert.equal(true, typeof a.createPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showSegmentDetails - errors', () => {
      it('should have a showSegmentDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showSegmentDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentId', (done) => {
        try {
          a.showSegmentDetails(null, (data, error) => {
            try {
              const displayE = 'segmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showSegmentDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSegment - errors', () => {
      it('should have a updateSegment function', (done) => {
        try {
          assert.equal(true, typeof a.updateSegment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentId', (done) => {
        try {
          a.updateSegment(null, null, (data, error) => {
            try {
              const displayE = 'segmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSegment - errors', () => {
      it('should have a deleteSegment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSegment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing segmentId', (done) => {
        try {
          a.deleteSegment(null, (data, error) => {
            try {
              const displayE = 'segmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteSegment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSegments - errors', () => {
      it('should have a listSegments function', (done) => {
        try {
          assert.equal(true, typeof a.listSegments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSegment - errors', () => {
      it('should have a createSegment function', (done) => {
        try {
          assert.equal(true, typeof a.createSegment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTrunks - errors', () => {
      it('should have a listTrunks function', (done) => {
        try {
          assert.equal(true, typeof a.listTrunks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTrunk - errors', () => {
      it('should have a createTrunk function', (done) => {
        try {
          assert.equal(true, typeof a.createTrunk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubportsToTrunk - errors', () => {
      it('should have a addSubportsToTrunk function', (done) => {
        try {
          assert.equal(true, typeof a.addSubportsToTrunk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trunkId', (done) => {
        try {
          a.addSubportsToTrunk(null, null, (data, error) => {
            try {
              const displayE = 'trunkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addSubportsToTrunk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubportsFromTrunk - errors', () => {
      it('should have a deleteSubportsFromTrunk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSubportsFromTrunk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trunkId', (done) => {
        try {
          a.deleteSubportsFromTrunk(null, null, (data, error) => {
            try {
              const displayE = 'trunkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteSubportsFromTrunk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSubportsForTrunk - errors', () => {
      it('should have a listSubportsForTrunk function', (done) => {
        try {
          assert.equal(true, typeof a.listSubportsForTrunk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trunkId', (done) => {
        try {
          a.listSubportsForTrunk(null, (data, error) => {
            try {
              const displayE = 'trunkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listSubportsForTrunk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTrunk - errors', () => {
      it('should have a updateTrunk function', (done) => {
        try {
          assert.equal(true, typeof a.updateTrunk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trunkId', (done) => {
        try {
          a.updateTrunk(null, null, (data, error) => {
            try {
              const displayE = 'trunkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateTrunk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showTrunk - errors', () => {
      it('should have a showTrunk function', (done) => {
        try {
          assert.equal(true, typeof a.showTrunk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trunkId', (done) => {
        try {
          a.showTrunk(null, (data, error) => {
            try {
              const displayE = 'trunkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showTrunk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTrunk - errors', () => {
      it('should have a deleteTrunk function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTrunk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trunkId', (done) => {
        try {
          a.deleteTrunk(null, (data, error) => {
            try {
              const displayE = 'trunkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteTrunk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAddressScope - errors', () => {
      it('should have a showAddressScope function', (done) => {
        try {
          assert.equal(true, typeof a.showAddressScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressScopeId', (done) => {
        try {
          a.showAddressScope('fakeparam', null, (data, error) => {
            try {
              const displayE = 'addressScopeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showAddressScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnAddressScope - errors', () => {
      it('should have a updateAnAddressScope function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnAddressScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressScopeId', (done) => {
        try {
          a.updateAnAddressScope(null, null, (data, error) => {
            try {
              const displayE = 'addressScopeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAnAddressScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAddressScope - errors', () => {
      it('should have a deleteAnAddressScope function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnAddressScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressScopeId', (done) => {
        try {
          a.deleteAnAddressScope(null, (data, error) => {
            try {
              const displayE = 'addressScopeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteAnAddressScope', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAddressScopes - errors', () => {
      it('should have a listAddressScopes function', (done) => {
        try {
          assert.equal(true, typeof a.listAddressScopes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAddressScope - errors', () => {
      it('should have a createAddressScope function', (done) => {
        try {
          assert.equal(true, typeof a.createAddressScope === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showConntrackHelper - errors', () => {
      it('should have a showConntrackHelper function', (done) => {
        try {
          assert.equal(true, typeof a.showConntrackHelper === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.showConntrackHelper('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showConntrackHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing conntrackHelperId', (done) => {
        try {
          a.showConntrackHelper('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'conntrackHelperId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showConntrackHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAConntrackHelper - errors', () => {
      it('should have a updateAConntrackHelper function', (done) => {
        try {
          assert.equal(true, typeof a.updateAConntrackHelper === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.updateAConntrackHelper(null, null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAConntrackHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing conntrackHelperId', (done) => {
        try {
          a.updateAConntrackHelper('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'conntrackHelperId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAConntrackHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAConntrackHelper - errors', () => {
      it('should have a deleteAConntrackHelper function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAConntrackHelper === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.deleteAConntrackHelper(null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteAConntrackHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing conntrackHelperId', (done) => {
        try {
          a.deleteAConntrackHelper('fakeparam', null, (data, error) => {
            try {
              const displayE = 'conntrackHelperId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteAConntrackHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRouterConntrackHelpers - errors', () => {
      it('should have a listRouterConntrackHelpers function', (done) => {
        try {
          assert.equal(true, typeof a.listRouterConntrackHelpers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.listRouterConntrackHelpers('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listRouterConntrackHelpers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createConntrackHelper - errors', () => {
      it('should have a createConntrackHelper function', (done) => {
        try {
          assert.equal(true, typeof a.createConntrackHelper === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.createConntrackHelper(null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createConntrackHelper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFloatingIPs - errors', () => {
      it('should have a listFloatingIPs function', (done) => {
        try {
          assert.equal(true, typeof a.listFloatingIPs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFloatingIP - errors', () => {
      it('should have a createFloatingIP function', (done) => {
        try {
          assert.equal(true, typeof a.createFloatingIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showFloatingIPDetails - errors', () => {
      it('should have a showFloatingIPDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showFloatingIPDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floatingipId', (done) => {
        try {
          a.showFloatingIPDetails(null, (data, error) => {
            try {
              const displayE = 'floatingipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showFloatingIPDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFloatingIP - errors', () => {
      it('should have a updateFloatingIP function', (done) => {
        try {
          assert.equal(true, typeof a.updateFloatingIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floatingipId', (done) => {
        try {
          a.updateFloatingIP(null, null, (data, error) => {
            try {
              const displayE = 'floatingipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateFloatingIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFloatingIP - errors', () => {
      it('should have a deleteFloatingIP function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFloatingIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floatingipId', (done) => {
        try {
          a.deleteFloatingIP(null, (data, error) => {
            try {
              const displayE = 'floatingipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteFloatingIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFloatingIPPools - errors', () => {
      it('should have a listFloatingIPPools function', (done) => {
        try {
          assert.equal(true, typeof a.listFloatingIPPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showPortForwarding - errors', () => {
      it('should have a showPortForwarding function', (done) => {
        try {
          assert.equal(true, typeof a.showPortForwarding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floatingipId', (done) => {
        try {
          a.showPortForwarding('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'floatingipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showPortForwarding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portForwardingId', (done) => {
        try {
          a.showPortForwarding('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'portForwardingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showPortForwarding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAPortForwarding - errors', () => {
      it('should have a updateAPortForwarding function', (done) => {
        try {
          assert.equal(true, typeof a.updateAPortForwarding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floatingipId', (done) => {
        try {
          a.updateAPortForwarding(null, null, null, (data, error) => {
            try {
              const displayE = 'floatingipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAPortForwarding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portForwardingId', (done) => {
        try {
          a.updateAPortForwarding('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'portForwardingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAPortForwarding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAFloatingIPPortForwarding - errors', () => {
      it('should have a deleteAFloatingIPPortForwarding function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAFloatingIPPortForwarding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floatingipId', (done) => {
        try {
          a.deleteAFloatingIPPortForwarding(null, null, (data, error) => {
            try {
              const displayE = 'floatingipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteAFloatingIPPortForwarding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portForwardingId', (done) => {
        try {
          a.deleteAFloatingIPPortForwarding('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portForwardingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteAFloatingIPPortForwarding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFloatingIPPortForwardings - errors', () => {
      it('should have a listFloatingIPPortForwardings function', (done) => {
        try {
          assert.equal(true, typeof a.listFloatingIPPortForwardings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floatingipId', (done) => {
        try {
          a.listFloatingIPPortForwardings('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'floatingipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listFloatingIPPortForwardings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPortForwarding - errors', () => {
      it('should have a createPortForwarding function', (done) => {
        try {
          assert.equal(true, typeof a.createPortForwarding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floatingipId', (done) => {
        try {
          a.createPortForwarding(null, null, (data, error) => {
            try {
              const displayE = 'floatingipId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createPortForwarding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRouters - errors', () => {
      it('should have a listRouters function', (done) => {
        try {
          assert.equal(true, typeof a.listRouters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRouter - errors', () => {
      it('should have a createRouter function', (done) => {
        try {
          assert.equal(true, typeof a.createRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showRouterDetails - errors', () => {
      it('should have a showRouterDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showRouterDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.showRouterDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showRouterDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRouter - errors', () => {
      it('should have a updateRouter function', (done) => {
        try {
          assert.equal(true, typeof a.updateRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.updateRouter(null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouter - errors', () => {
      it('should have a deleteRouter function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.deleteRouter(null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInterfaceToRouter - errors', () => {
      it('should have a addInterfaceToRouter function', (done) => {
        try {
          assert.equal(true, typeof a.addInterfaceToRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.addInterfaceToRouter(null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addInterfaceToRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeInterfaceFromRouter - errors', () => {
      it('should have a removeInterfaceFromRouter function', (done) => {
        try {
          assert.equal(true, typeof a.removeInterfaceFromRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.removeInterfaceFromRouter(null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeInterfaceFromRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addExtraRoutesToRouter - errors', () => {
      it('should have a addExtraRoutesToRouter function', (done) => {
        try {
          assert.equal(true, typeof a.addExtraRoutesToRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.addExtraRoutesToRouter(null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addExtraRoutesToRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeExtraRoutesFromRouter - errors', () => {
      it('should have a removeExtraRoutesFromRouter function', (done) => {
        try {
          assert.equal(true, typeof a.removeExtraRoutesFromRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.removeExtraRoutesFromRouter(null, null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeExtraRoutesFromRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNdpProxies - errors', () => {
      it('should have a listNdpProxies function', (done) => {
        try {
          assert.equal(true, typeof a.listNdpProxies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNdpProxy - errors', () => {
      it('should have a createNdpProxy function', (done) => {
        try {
          assert.equal(true, typeof a.createNdpProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showNdpProxyDetails - errors', () => {
      it('should have a showNdpProxyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showNdpProxyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ndpProxyId', (done) => {
        try {
          a.showNdpProxyDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ndpProxyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showNdpProxyDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateANdpProxy - errors', () => {
      it('should have a updateANdpProxy function', (done) => {
        try {
          assert.equal(true, typeof a.updateANdpProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ndpProxyId', (done) => {
        try {
          a.updateANdpProxy(null, null, (data, error) => {
            try {
              const displayE = 'ndpProxyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateANdpProxy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteANdpProxy - errors', () => {
      it('should have a deleteANdpProxy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteANdpProxy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ndpProxyId', (done) => {
        try {
          a.deleteANdpProxy(null, (data, error) => {
            try {
              const displayE = 'ndpProxyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteANdpProxy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showSubnetPool - errors', () => {
      it('should have a showSubnetPool function', (done) => {
        try {
          assert.equal(true, typeof a.showSubnetPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetpoolId', (done) => {
        try {
          a.showSubnetPool('fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetpoolId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showSubnetPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSubnetPool - errors', () => {
      it('should have a updateSubnetPool function', (done) => {
        try {
          assert.equal(true, typeof a.updateSubnetPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetpoolId', (done) => {
        try {
          a.updateSubnetPool(null, null, (data, error) => {
            try {
              const displayE = 'subnetpoolId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateSubnetPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubnetPool - errors', () => {
      it('should have a deleteSubnetPool function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSubnetPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetpoolId', (done) => {
        try {
          a.deleteSubnetPool(null, (data, error) => {
            try {
              const displayE = 'subnetpoolId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteSubnetPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSubnetPools - errors', () => {
      it('should have a listSubnetPools function', (done) => {
        try {
          assert.equal(true, typeof a.listSubnetPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSubnetPool - errors', () => {
      it('should have a createSubnetPool function', (done) => {
        try {
          assert.equal(true, typeof a.createSubnetPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPrefixes - errors', () => {
      it('should have a addPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.addPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetpoolId', (done) => {
        try {
          a.addPrefixes(null, null, (data, error) => {
            try {
              const displayE = 'subnetpoolId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addPrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removePrefixes - errors', () => {
      it('should have a removePrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.removePrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetpoolId', (done) => {
        try {
          a.removePrefixes(null, null, (data, error) => {
            try {
              const displayE = 'subnetpoolId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removePrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSubnets - errors', () => {
      it('should have a listSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.listSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSubnet - errors', () => {
      it('should have a createSubnet function', (done) => {
        try {
          assert.equal(true, typeof a.createSubnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showSubnetDetails - errors', () => {
      it('should have a showSubnetDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showSubnetDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.showSubnetDetails(null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showSubnetDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSubnet - errors', () => {
      it('should have a updateSubnet function', (done) => {
        try {
          assert.equal(true, typeof a.updateSubnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.updateSubnet(null, null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateSubnet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubnet - errors', () => {
      it('should have a deleteSubnet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSubnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.deleteSubnet(null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteSubnet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLocalIPs - errors', () => {
      it('should have a listLocalIPs function', (done) => {
        try {
          assert.equal(true, typeof a.listLocalIPs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLocalIP - errors', () => {
      it('should have a createLocalIP function', (done) => {
        try {
          assert.equal(true, typeof a.createLocalIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showLocalIPDetails - errors', () => {
      it('should have a showLocalIPDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showLocalIPDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localIpId', (done) => {
        try {
          a.showLocalIPDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'localIpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showLocalIPDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLocalIP - errors', () => {
      it('should have a updateLocalIP function', (done) => {
        try {
          assert.equal(true, typeof a.updateLocalIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localIpId', (done) => {
        try {
          a.updateLocalIP(null, null, (data, error) => {
            try {
              const displayE = 'localIpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateLocalIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLocalIP - errors', () => {
      it('should have a deleteLocalIP function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLocalIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localIpId', (done) => {
        try {
          a.deleteLocalIP(null, (data, error) => {
            try {
              const displayE = 'localIpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteLocalIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLocalIPAssociations - errors', () => {
      it('should have a listLocalIPAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.listLocalIPAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localIpId', (done) => {
        try {
          a.listLocalIPAssociations('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'localIpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listLocalIPAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLocalIPAssociation - errors', () => {
      it('should have a createLocalIPAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.createLocalIPAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localIpId', (done) => {
        try {
          a.createLocalIPAssociation(null, null, (data, error) => {
            try {
              const displayE = 'localIpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createLocalIPAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLocalIPAssociation - errors', () => {
      it('should have a deleteLocalIPAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLocalIPAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing localIpId', (done) => {
        try {
          a.deleteLocalIPAssociation(null, null, (data, error) => {
            try {
              const displayE = 'localIpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteLocalIPAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fixedPortId', (done) => {
        try {
          a.deleteLocalIPAssociation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'fixedPortId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteLocalIPAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAddressGroup - errors', () => {
      it('should have a showAddressGroup function', (done) => {
        try {
          assert.equal(true, typeof a.showAddressGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressGroupId', (done) => {
        try {
          a.showAddressGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'addressGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showAddressGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAddressGroup - errors', () => {
      it('should have a createAddressGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createAddressGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressGroupId', (done) => {
        try {
          a.createAddressGroup(null, null, (data, error) => {
            try {
              const displayE = 'addressGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createAddressGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnAddressGroup - errors', () => {
      it('should have a updateAnAddressGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnAddressGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressGroupId', (done) => {
        try {
          a.updateAnAddressGroup(null, null, (data, error) => {
            try {
              const displayE = 'addressGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAnAddressGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAddressGroup - errors', () => {
      it('should have a deleteAnAddressGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnAddressGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressGroupId', (done) => {
        try {
          a.deleteAnAddressGroup(null, (data, error) => {
            try {
              const displayE = 'addressGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteAnAddressGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAddressGroups - errors', () => {
      it('should have a listAddressGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listAddressGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIPAddressesToAddressGroup - errors', () => {
      it('should have a addIPAddressesToAddressGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addIPAddressesToAddressGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressGroupId', (done) => {
        try {
          a.addIPAddressesToAddressGroup(null, null, (data, error) => {
            try {
              const displayE = 'addressGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addIPAddressesToAddressGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIPAddressesToAddressGroup - errors', () => {
      it('should have a removeIPAddressesToAddressGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeIPAddressesToAddressGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addressGroupId', (done) => {
        try {
          a.removeIPAddressesToAddressGroup(null, null, (data, error) => {
            try {
              const displayE = 'addressGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeIPAddressesToAddressGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFirewallGroups - errors', () => {
      it('should have a listFirewallGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listFirewallGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFirewallGroup - errors', () => {
      it('should have a createFirewallGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createFirewallGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showFirewallGroupDetails - errors', () => {
      it('should have a showFirewallGroupDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showFirewallGroupDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallGroupId', (done) => {
        try {
          a.showFirewallGroupDetails(null, (data, error) => {
            try {
              const displayE = 'firewallGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showFirewallGroupDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFirewallGroup - errors', () => {
      it('should have a updateFirewallGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateFirewallGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallGroupId', (done) => {
        try {
          a.updateFirewallGroup(null, null, (data, error) => {
            try {
              const displayE = 'firewallGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateFirewallGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFirewallGroup - errors', () => {
      it('should have a deleteFirewallGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFirewallGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallGroupId', (done) => {
        try {
          a.deleteFirewallGroup(null, (data, error) => {
            try {
              const displayE = 'firewallGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteFirewallGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFirewallPolicies - errors', () => {
      it('should have a listFirewallPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listFirewallPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFirewallPolicy - errors', () => {
      it('should have a createFirewallPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createFirewallPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showFirewallPolicyDetails - errors', () => {
      it('should have a showFirewallPolicyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showFirewallPolicyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyId', (done) => {
        try {
          a.showFirewallPolicyDetails(null, (data, error) => {
            try {
              const displayE = 'firewallPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showFirewallPolicyDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFirewallPolicy - errors', () => {
      it('should have a updateFirewallPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateFirewallPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyId', (done) => {
        try {
          a.updateFirewallPolicy(null, null, (data, error) => {
            try {
              const displayE = 'firewallPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateFirewallPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFirewallPolicy - errors', () => {
      it('should have a deleteFirewallPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFirewallPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyId', (done) => {
        try {
          a.deleteFirewallPolicy(null, (data, error) => {
            try {
              const displayE = 'firewallPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteFirewallPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFirewallRules - errors', () => {
      it('should have a listFirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.listFirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFirewallRule - errors', () => {
      it('should have a createFirewallRule function', (done) => {
        try {
          assert.equal(true, typeof a.createFirewallRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showFirewallRuleDetails - errors', () => {
      it('should have a showFirewallRuleDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showFirewallRuleDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallRuleId', (done) => {
        try {
          a.showFirewallRuleDetails(null, (data, error) => {
            try {
              const displayE = 'firewallRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showFirewallRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFirewallRule - errors', () => {
      it('should have a updateFirewallRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateFirewallRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallRuleId', (done) => {
        try {
          a.updateFirewallRule(null, null, (data, error) => {
            try {
              const displayE = 'firewallRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateFirewallRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFirewallRule - errors', () => {
      it('should have a deleteFirewallRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFirewallRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallRuleId', (done) => {
        try {
          a.deleteFirewallRule(null, (data, error) => {
            try {
              const displayE = 'firewallRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteFirewallRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#insertRuleIntoAFirewallPolicy - errors', () => {
      it('should have a insertRuleIntoAFirewallPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.insertRuleIntoAFirewallPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyId', (done) => {
        try {
          a.insertRuleIntoAFirewallPolicy(null, null, (data, error) => {
            try {
              const displayE = 'firewallPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-insertRuleIntoAFirewallPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeRuleFromFirewallPolicy - errors', () => {
      it('should have a removeRuleFromFirewallPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.removeRuleFromFirewallPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallPolicyId', (done) => {
        try {
          a.removeRuleFromFirewallPolicy(null, null, (data, error) => {
            try {
              const displayE = 'firewallPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeRuleFromFirewallPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showRBACPolicyDetails - errors', () => {
      it('should have a showRBACPolicyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showRBACPolicyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rbacPolicyId', (done) => {
        try {
          a.showRBACPolicyDetails(null, (data, error) => {
            try {
              const displayE = 'rbacPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showRBACPolicyDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRBACPolicy - errors', () => {
      it('should have a updateRBACPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateRBACPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rbacPolicyId', (done) => {
        try {
          a.updateRBACPolicy(null, null, (data, error) => {
            try {
              const displayE = 'rbacPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateRBACPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRBACPolicy - errors', () => {
      it('should have a deleteRBACPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRBACPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rbacPolicyId', (done) => {
        try {
          a.deleteRBACPolicy(null, (data, error) => {
            try {
              const displayE = 'rbacPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteRBACPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRBACPolicies - errors', () => {
      it('should have a listRBACPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listRBACPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRBACPolicy - errors', () => {
      it('should have a createRBACPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createRBACPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSecurityGroupRules - errors', () => {
      it('should have a listSecurityGroupRules function', (done) => {
        try {
          assert.equal(true, typeof a.listSecurityGroupRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSecurityGroupRule - errors', () => {
      it('should have a createSecurityGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.createSecurityGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showSecurityGroupRule - errors', () => {
      it('should have a showSecurityGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.showSecurityGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupRuleId', (done) => {
        try {
          a.showSecurityGroupRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'securityGroupRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showSecurityGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityGroupRule - errors', () => {
      it('should have a deleteSecurityGroupRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityGroupRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupRuleId', (done) => {
        try {
          a.deleteSecurityGroupRule(null, (data, error) => {
            try {
              const displayE = 'securityGroupRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteSecurityGroupRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSecurityGroups - errors', () => {
      it('should have a listSecurityGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listSecurityGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSecurityGroup - errors', () => {
      it('should have a createSecurityGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createSecurityGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showSecurityGroup - errors', () => {
      it('should have a showSecurityGroup function', (done) => {
        try {
          assert.equal(true, typeof a.showSecurityGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupId', (done) => {
        try {
          a.showSecurityGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'securityGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showSecurityGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecurityGroup - errors', () => {
      it('should have a updateSecurityGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateSecurityGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupId', (done) => {
        try {
          a.updateSecurityGroup(null, null, (data, error) => {
            try {
              const displayE = 'securityGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateSecurityGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityGroup - errors', () => {
      it('should have a deleteSecurityGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupId', (done) => {
        try {
          a.deleteSecurityGroup(null, (data, error) => {
            try {
              const displayE = 'securityGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteSecurityGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIKEPolicies - errors', () => {
      it('should have a listIKEPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listIKEPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIKEPolicy - errors', () => {
      it('should have a createIKEPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createIKEPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showIKEPolicyDetails - errors', () => {
      it('should have a showIKEPolicyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showIKEPolicyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ikepolicyId', (done) => {
        try {
          a.showIKEPolicyDetails(null, (data, error) => {
            try {
              const displayE = 'ikepolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showIKEPolicyDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIKEPolicy - errors', () => {
      it('should have a updateIKEPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateIKEPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ikepolicyId', (done) => {
        try {
          a.updateIKEPolicy(null, null, (data, error) => {
            try {
              const displayE = 'ikepolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateIKEPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIKEPolicy - errors', () => {
      it('should have a removeIKEPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.removeIKEPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ikepolicyId', (done) => {
        try {
          a.removeIKEPolicy(null, (data, error) => {
            try {
              const displayE = 'ikepolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeIKEPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIPsecPolicies - errors', () => {
      it('should have a listIPsecPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listIPsecPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIPsecPolicy - errors', () => {
      it('should have a createIPsecPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createIPsecPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showIPsecPolicy - errors', () => {
      it('should have a showIPsecPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.showIPsecPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsecpolicyId', (done) => {
        try {
          a.showIPsecPolicy(null, (data, error) => {
            try {
              const displayE = 'ipsecpolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showIPsecPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIPsecPolicy - errors', () => {
      it('should have a updateIPsecPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateIPsecPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsecpolicyId', (done) => {
        try {
          a.updateIPsecPolicy(null, null, (data, error) => {
            try {
              const displayE = 'ipsecpolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateIPsecPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIPsecPolicy - errors', () => {
      it('should have a removeIPsecPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.removeIPsecPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipsecpolicyId', (done) => {
        try {
          a.removeIPsecPolicy(null, (data, error) => {
            try {
              const displayE = 'ipsecpolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeIPsecPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIPsecConnections - errors', () => {
      it('should have a listIPsecConnections function', (done) => {
        try {
          assert.equal(true, typeof a.listIPsecConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIPsecConnection - errors', () => {
      it('should have a createIPsecConnection function', (done) => {
        try {
          assert.equal(true, typeof a.createIPsecConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showIPsecConnection - errors', () => {
      it('should have a showIPsecConnection function', (done) => {
        try {
          assert.equal(true, typeof a.showIPsecConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionId', (done) => {
        try {
          a.showIPsecConnection(null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showIPsecConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIPsecConnection - errors', () => {
      it('should have a updateIPsecConnection function', (done) => {
        try {
          assert.equal(true, typeof a.updateIPsecConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionId', (done) => {
        try {
          a.updateIPsecConnection(null, null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateIPsecConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIPsecConnection - errors', () => {
      it('should have a removeIPsecConnection function', (done) => {
        try {
          assert.equal(true, typeof a.removeIPsecConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionId', (done) => {
        try {
          a.removeIPsecConnection(null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeIPsecConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVPNEndpointGroups - errors', () => {
      it('should have a listVPNEndpointGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listVPNEndpointGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVPNEndpointGroup - errors', () => {
      it('should have a createVPNEndpointGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createVPNEndpointGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVPNEndpointGroup - errors', () => {
      it('should have a showVPNEndpointGroup function', (done) => {
        try {
          assert.equal(true, typeof a.showVPNEndpointGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointGroupId', (done) => {
        try {
          a.showVPNEndpointGroup(null, (data, error) => {
            try {
              const displayE = 'endpointGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showVPNEndpointGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVPNEndpointGroup - errors', () => {
      it('should have a updateVPNEndpointGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateVPNEndpointGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointGroupId', (done) => {
        try {
          a.updateVPNEndpointGroup(null, null, (data, error) => {
            try {
              const displayE = 'endpointGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateVPNEndpointGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeVPNEndpointGroup - errors', () => {
      it('should have a removeVPNEndpointGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeVPNEndpointGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointGroupId', (done) => {
        try {
          a.removeVPNEndpointGroup(null, (data, error) => {
            try {
              const displayE = 'endpointGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeVPNEndpointGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVPNServices - errors', () => {
      it('should have a listVPNServices function', (done) => {
        try {
          assert.equal(true, typeof a.listVPNServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVPNService - errors', () => {
      it('should have a createVPNService function', (done) => {
        try {
          assert.equal(true, typeof a.createVPNService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVPNServiceDetails - errors', () => {
      it('should have a showVPNServiceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showVPNServiceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.showVPNServiceDetails(null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showVPNServiceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateVPNService - errors', () => {
      it('should have a updateVPNService function', (done) => {
        try {
          assert.equal(true, typeof a.updateVPNService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.updateVPNService(null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateVPNService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeVPNService - errors', () => {
      it('should have a removeVPNService function', (done) => {
        try {
          assert.equal(true, typeof a.removeVPNService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.removeVPNService(null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeVPNService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFlavors - errors', () => {
      it('should have a listFlavors function', (done) => {
        try {
          assert.equal(true, typeof a.listFlavors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFlavor - errors', () => {
      it('should have a createFlavor function', (done) => {
        try {
          assert.equal(true, typeof a.createFlavor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showFlavorDetails - errors', () => {
      it('should have a showFlavorDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showFlavorDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flavorId', (done) => {
        try {
          a.showFlavorDetails(null, (data, error) => {
            try {
              const displayE = 'flavorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showFlavorDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFlavor - errors', () => {
      it('should have a updateFlavor function', (done) => {
        try {
          assert.equal(true, typeof a.updateFlavor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flavorId', (done) => {
        try {
          a.updateFlavor(null, null, (data, error) => {
            try {
              const displayE = 'flavorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateFlavor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFlavor - errors', () => {
      it('should have a deleteFlavor function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFlavor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flavorId', (done) => {
        try {
          a.deleteFlavor(null, (data, error) => {
            try {
              const displayE = 'flavorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteFlavor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateFlavorWithAServiceProfile - errors', () => {
      it('should have a associateFlavorWithAServiceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.associateFlavorWithAServiceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flavorId', (done) => {
        try {
          a.associateFlavorWithAServiceProfile(null, null, (data, error) => {
            try {
              const displayE = 'flavorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-associateFlavorWithAServiceProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateAFlavor - errors', () => {
      it('should have a disassociateAFlavor function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateAFlavor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.disassociateAFlavor(null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-disassociateAFlavor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flavorId', (done) => {
        try {
          a.disassociateAFlavor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'flavorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-disassociateAFlavor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listServiceProfiles - errors', () => {
      it('should have a listServiceProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.listServiceProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createServiceProfile - errors', () => {
      it('should have a createServiceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createServiceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showServiceProfileDetails - errors', () => {
      it('should have a showServiceProfileDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showServiceProfileDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.showServiceProfileDetails(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showServiceProfileDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateServiceProfile - errors', () => {
      it('should have a updateServiceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateServiceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.updateServiceProfile(null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateServiceProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceProfile - errors', () => {
      it('should have a deleteServiceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServiceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.deleteServiceProfile(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteServiceProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMeteringLabels - errors', () => {
      it('should have a listMeteringLabels function', (done) => {
        try {
          assert.equal(true, typeof a.listMeteringLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMeteringLabel - errors', () => {
      it('should have a createMeteringLabel function', (done) => {
        try {
          assert.equal(true, typeof a.createMeteringLabel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showMeteringLabelDetails - errors', () => {
      it('should have a showMeteringLabelDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showMeteringLabelDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meteringLabelId', (done) => {
        try {
          a.showMeteringLabelDetails(null, (data, error) => {
            try {
              const displayE = 'meteringLabelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showMeteringLabelDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMeteringLabel - errors', () => {
      it('should have a deleteMeteringLabel function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMeteringLabel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meteringLabelId', (done) => {
        try {
          a.deleteMeteringLabel(null, (data, error) => {
            try {
              const displayE = 'meteringLabelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteMeteringLabel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMeteringLabelRules - errors', () => {
      it('should have a listMeteringLabelRules function', (done) => {
        try {
          assert.equal(true, typeof a.listMeteringLabelRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMeteringLabelRule - errors', () => {
      it('should have a createMeteringLabelRule function', (done) => {
        try {
          assert.equal(true, typeof a.createMeteringLabelRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showMeteringLabelRuleDetails - errors', () => {
      it('should have a showMeteringLabelRuleDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showMeteringLabelRuleDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meteringLabelRuleId', (done) => {
        try {
          a.showMeteringLabelRuleDetails(null, (data, error) => {
            try {
              const displayE = 'meteringLabelRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showMeteringLabelRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMeteringLabelRule - errors', () => {
      it('should have a deleteMeteringLabelRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMeteringLabelRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meteringLabelRuleId', (done) => {
        try {
          a.deleteMeteringLabelRule(null, (data, error) => {
            try {
              const displayE = 'meteringLabelRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteMeteringLabelRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showNetworkIPAvailability - errors', () => {
      it('should have a showNetworkIPAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.showNetworkIPAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.showNetworkIPAvailability(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showNetworkIPAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkIPAvailability - errors', () => {
      it('should have a listNetworkIPAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkIPAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listQuotasForProjectsWithNonDefaultQuotaValues - errors', () => {
      it('should have a listQuotasForProjectsWithNonDefaultQuotaValues function', (done) => {
        try {
          assert.equal(true, typeof a.listQuotasForProjectsWithNonDefaultQuotaValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listQuotasForAProject - errors', () => {
      it('should have a listQuotasForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.listQuotasForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listQuotasForAProject(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateQuotaForAProject - errors', () => {
      it('should have a updateQuotaForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.updateQuotaForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.updateQuotaForAProject(null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateQuotaForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetQuotaForAProject - errors', () => {
      it('should have a resetQuotaForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.resetQuotaForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.resetQuotaForAProject(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-resetQuotaForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultQuotasForAProject - errors', () => {
      it('should have a listDefaultQuotasForAProject function', (done) => {
        try {
          assert.equal(true, typeof a.listDefaultQuotasForAProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.listDefaultQuotasForAProject(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listDefaultQuotasForAProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showQuotaDetailsForATenant - errors', () => {
      it('should have a showQuotaDetailsForATenant function', (done) => {
        try {
          assert.equal(true, typeof a.showQuotaDetailsForATenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showQuotaDetailsForATenant(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showQuotaDetailsForATenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listServiceProviders - errors', () => {
      it('should have a listServiceProviders function', (done) => {
        try {
          assert.equal(true, typeof a.listServiceProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAllTags - errors', () => {
      it('should have a replaceAllTags function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAllTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.replaceAllTags(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-replaceAllTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.replaceAllTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-replaceAllTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeAllTags - errors', () => {
      it('should have a removeAllTags function', (done) => {
        try {
          assert.equal(true, typeof a.removeAllTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.removeAllTags(null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeAllTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.removeAllTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeAllTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#obtainTagList - errors', () => {
      it('should have a obtainTagList function', (done) => {
        try {
          assert.equal(true, typeof a.obtainTagList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.obtainTagList(null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-obtainTagList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.obtainTagList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-obtainTagList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#confirmATag - errors', () => {
      it('should have a confirmATag function', (done) => {
        try {
          assert.equal(true, typeof a.confirmATag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.confirmATag(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-confirmATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.confirmATag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-confirmATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tag', (done) => {
        try {
          a.confirmATag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-confirmATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addATag - errors', () => {
      it('should have a addATag function', (done) => {
        try {
          assert.equal(true, typeof a.addATag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.addATag(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.addATag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tag', (done) => {
        try {
          a.addATag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeATag - errors', () => {
      it('should have a removeATag function', (done) => {
        try {
          assert.equal(true, typeof a.removeATag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.removeATag(null, null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.removeATag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tag', (done) => {
        try {
          a.removeATag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeATag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listQoSRuleTypes - errors', () => {
      it('should have a listQoSRuleTypes function', (done) => {
        try {
          assert.equal(true, typeof a.listQoSRuleTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing allSupported', (done) => {
        try {
          a.listQoSRuleTypes(null, null, (data, error) => {
            try {
              const displayE = 'allSupported is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listQoSRuleTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing allRules', (done) => {
        try {
          a.listQoSRuleTypes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'allRules is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listQoSRuleTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showQoSRuleTypeDetails - errors', () => {
      it('should have a showQoSRuleTypeDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showQoSRuleTypeDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleType', (done) => {
        try {
          a.showQoSRuleTypeDetails(null, (data, error) => {
            try {
              const displayE = 'ruleType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showQoSRuleTypeDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listQoSPolicies - errors', () => {
      it('should have a listQoSPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.listQoSPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createQoSPolicy - errors', () => {
      it('should have a createQoSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createQoSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showQoSPolicyDetails - errors', () => {
      it('should have a showQoSPolicyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showQoSPolicyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.showQoSPolicyDetails(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showQoSPolicyDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateQoSPolicy - errors', () => {
      it('should have a updateQoSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateQoSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateQoSPolicy(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateQoSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteQoSPolicy - errors', () => {
      it('should have a deleteQoSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteQoSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteQoSPolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteQoSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBandwidthLimitRulesForQoSPolicy - errors', () => {
      it('should have a listBandwidthLimitRulesForQoSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listBandwidthLimitRulesForQoSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.listBandwidthLimitRulesForQoSPolicy('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listBandwidthLimitRulesForQoSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBandwidthLimitRule - errors', () => {
      it('should have a createBandwidthLimitRule function', (done) => {
        try {
          assert.equal(true, typeof a.createBandwidthLimitRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.createBandwidthLimitRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createBandwidthLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showBandwidthLimitRuleDetails - errors', () => {
      it('should have a showBandwidthLimitRuleDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showBandwidthLimitRuleDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.showBandwidthLimitRuleDetails(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showBandwidthLimitRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.showBandwidthLimitRuleDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showBandwidthLimitRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBandwidthLimitRule - errors', () => {
      it('should have a updateBandwidthLimitRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateBandwidthLimitRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateBandwidthLimitRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateBandwidthLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateBandwidthLimitRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateBandwidthLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBandwidthLimitRule - errors', () => {
      it('should have a deleteBandwidthLimitRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBandwidthLimitRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteBandwidthLimitRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteBandwidthLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteBandwidthLimitRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteBandwidthLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDSCPMarkingRulesForQoSPolicy - errors', () => {
      it('should have a listDSCPMarkingRulesForQoSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listDSCPMarkingRulesForQoSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.listDSCPMarkingRulesForQoSPolicy('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listDSCPMarkingRulesForQoSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDSCPMarkingRule - errors', () => {
      it('should have a createDSCPMarkingRule function', (done) => {
        try {
          assert.equal(true, typeof a.createDSCPMarkingRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.createDSCPMarkingRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createDSCPMarkingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showDSCPMarkingRuleDetails - errors', () => {
      it('should have a showDSCPMarkingRuleDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showDSCPMarkingRuleDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.showDSCPMarkingRuleDetails(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showDSCPMarkingRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscpRuleId', (done) => {
        try {
          a.showDSCPMarkingRuleDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'dscpRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showDSCPMarkingRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDSCPMarkingRule - errors', () => {
      it('should have a updateDSCPMarkingRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateDSCPMarkingRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateDSCPMarkingRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateDSCPMarkingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscpRuleId', (done) => {
        try {
          a.updateDSCPMarkingRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'dscpRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateDSCPMarkingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDSCPMarkingRule - errors', () => {
      it('should have a deleteDSCPMarkingRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDSCPMarkingRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteDSCPMarkingRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteDSCPMarkingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscpRuleId', (done) => {
        try {
          a.deleteDSCPMarkingRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'dscpRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteDSCPMarkingRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMinimumBandwidthRulesForQoSPolicy - errors', () => {
      it('should have a listMinimumBandwidthRulesForQoSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listMinimumBandwidthRulesForQoSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.listMinimumBandwidthRulesForQoSPolicy('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listMinimumBandwidthRulesForQoSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMinimumBandwidthRule - errors', () => {
      it('should have a createMinimumBandwidthRule function', (done) => {
        try {
          assert.equal(true, typeof a.createMinimumBandwidthRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.createMinimumBandwidthRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createMinimumBandwidthRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showMinimumBandwidthRuleDetails - errors', () => {
      it('should have a showMinimumBandwidthRuleDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showMinimumBandwidthRuleDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.showMinimumBandwidthRuleDetails(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showMinimumBandwidthRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.showMinimumBandwidthRuleDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showMinimumBandwidthRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMinimumBandwidthRule - errors', () => {
      it('should have a updateMinimumBandwidthRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateMinimumBandwidthRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateMinimumBandwidthRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateMinimumBandwidthRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateMinimumBandwidthRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateMinimumBandwidthRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMinimumBandwidthRule - errors', () => {
      it('should have a deleteMinimumBandwidthRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMinimumBandwidthRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteMinimumBandwidthRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteMinimumBandwidthRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteMinimumBandwidthRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteMinimumBandwidthRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMinimumPacketRateRulesForQoSPolicy - errors', () => {
      it('should have a listMinimumPacketRateRulesForQoSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listMinimumPacketRateRulesForQoSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.listMinimumPacketRateRulesForQoSPolicy('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listMinimumPacketRateRulesForQoSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMinimumPacketRateRule - errors', () => {
      it('should have a createMinimumPacketRateRule function', (done) => {
        try {
          assert.equal(true, typeof a.createMinimumPacketRateRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.createMinimumPacketRateRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createMinimumPacketRateRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showMinimumPacketRateRuleDetails - errors', () => {
      it('should have a showMinimumPacketRateRuleDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showMinimumPacketRateRuleDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.showMinimumPacketRateRuleDetails(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showMinimumPacketRateRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.showMinimumPacketRateRuleDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showMinimumPacketRateRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMinimumPacketRateRule - errors', () => {
      it('should have a updateMinimumPacketRateRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateMinimumPacketRateRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateMinimumPacketRateRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateMinimumPacketRateRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateMinimumPacketRateRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateMinimumPacketRateRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMinimumPacketRateRule - errors', () => {
      it('should have a deleteMinimumPacketRateRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMinimumPacketRateRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteMinimumPacketRateRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteMinimumPacketRateRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteMinimumPacketRateRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteMinimumPacketRateRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPacketRateLimitRulesForQoSPolicy - errors', () => {
      it('should have a listPacketRateLimitRulesForQoSPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.listPacketRateLimitRulesForQoSPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.listPacketRateLimitRulesForQoSPolicy('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listPacketRateLimitRulesForQoSPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPacketRateLimitRule - errors', () => {
      it('should have a createPacketRateLimitRule function', (done) => {
        try {
          assert.equal(true, typeof a.createPacketRateLimitRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.createPacketRateLimitRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createPacketRateLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showPacketRateLimitRuleDetails - errors', () => {
      it('should have a showPacketRateLimitRuleDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showPacketRateLimitRuleDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.showPacketRateLimitRuleDetails(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showPacketRateLimitRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.showPacketRateLimitRuleDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showPacketRateLimitRuleDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePacketRateLimitRule - errors', () => {
      it('should have a updatePacketRateLimitRule function', (done) => {
        try {
          assert.equal(true, typeof a.updatePacketRateLimitRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updatePacketRateLimitRule(null, null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updatePacketRateLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updatePacketRateLimitRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updatePacketRateLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePacketRateLimitRule - errors', () => {
      it('should have a deletePacketRateLimitRule function', (done) => {
        try {
          assert.equal(true, typeof a.deletePacketRateLimitRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deletePacketRateLimitRule(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deletePacketRateLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deletePacketRateLimitRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deletePacketRateLimitRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showBandwidthLimitRuleDetailsAlias - errors', () => {
      it('should have a showBandwidthLimitRuleDetailsAlias function', (done) => {
        try {
          assert.equal(true, typeof a.showBandwidthLimitRuleDetailsAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.showBandwidthLimitRuleDetailsAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showBandwidthLimitRuleDetailsAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBandwidthLimitRuleAlias - errors', () => {
      it('should have a updateBandwidthLimitRuleAlias function', (done) => {
        try {
          assert.equal(true, typeof a.updateBandwidthLimitRuleAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateBandwidthLimitRuleAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateBandwidthLimitRuleAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBandwidthLimitRuleAlias - errors', () => {
      it('should have a deleteBandwidthLimitRuleAlias function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBandwidthLimitRuleAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteBandwidthLimitRuleAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteBandwidthLimitRuleAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showDSCPMarkingRuleDetailsAlias - errors', () => {
      it('should have a showDSCPMarkingRuleDetailsAlias function', (done) => {
        try {
          assert.equal(true, typeof a.showDSCPMarkingRuleDetailsAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscpRuleId', (done) => {
        try {
          a.showDSCPMarkingRuleDetailsAlias(null, (data, error) => {
            try {
              const displayE = 'dscpRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showDSCPMarkingRuleDetailsAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDSCPMarkingRuleAlias - errors', () => {
      it('should have a updateDSCPMarkingRuleAlias function', (done) => {
        try {
          assert.equal(true, typeof a.updateDSCPMarkingRuleAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscpRuleId', (done) => {
        try {
          a.updateDSCPMarkingRuleAlias(null, (data, error) => {
            try {
              const displayE = 'dscpRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateDSCPMarkingRuleAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDSCPMarkingRuleAlias - errors', () => {
      it('should have a deleteDSCPMarkingRuleAlias function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDSCPMarkingRuleAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscpRuleId', (done) => {
        try {
          a.deleteDSCPMarkingRuleAlias(null, (data, error) => {
            try {
              const displayE = 'dscpRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteDSCPMarkingRuleAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showMinimumBandwidthRuleDetailsAlias - errors', () => {
      it('should have a showMinimumBandwidthRuleDetailsAlias function', (done) => {
        try {
          assert.equal(true, typeof a.showMinimumBandwidthRuleDetailsAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.showMinimumBandwidthRuleDetailsAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showMinimumBandwidthRuleDetailsAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMinimumBandwidthRuleAlias - errors', () => {
      it('should have a updateMinimumBandwidthRuleAlias function', (done) => {
        try {
          assert.equal(true, typeof a.updateMinimumBandwidthRuleAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateMinimumBandwidthRuleAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateMinimumBandwidthRuleAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMinimumBandwidthRuleAlias - errors', () => {
      it('should have a deleteMinimumBandwidthRuleAlias function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMinimumBandwidthRuleAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteMinimumBandwidthRuleAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteMinimumBandwidthRuleAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showMinimumPacketRateRuleDetailsAlias - errors', () => {
      it('should have a showMinimumPacketRateRuleDetailsAlias function', (done) => {
        try {
          assert.equal(true, typeof a.showMinimumPacketRateRuleDetailsAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.showMinimumPacketRateRuleDetailsAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showMinimumPacketRateRuleDetailsAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMinimumPacketRateRuleAlias - errors', () => {
      it('should have a updateMinimumPacketRateRuleAlias function', (done) => {
        try {
          assert.equal(true, typeof a.updateMinimumPacketRateRuleAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.updateMinimumPacketRateRuleAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateMinimumPacketRateRuleAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMinimumPacketRateRuleAlias - errors', () => {
      it('should have a deleteMinimumPacketRateRuleAlias function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMinimumPacketRateRuleAlias === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleId', (done) => {
        try {
          a.deleteMinimumPacketRateRuleAlias(null, (data, error) => {
            try {
              const displayE = 'ruleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteMinimumPacketRateRuleAlias', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLoggingResources - errors', () => {
      it('should have a listLoggingResources function', (done) => {
        try {
          assert.equal(true, typeof a.listLoggingResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLoggingResource - errors', () => {
      it('should have a createLoggingResource function', (done) => {
        try {
          assert.equal(true, typeof a.createLoggingResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showLoggingResourceDetails - errors', () => {
      it('should have a showLoggingResourceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showLoggingResourceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggingResourceId', (done) => {
        try {
          a.showLoggingResourceDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loggingResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showLoggingResourceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLoggingResource - errors', () => {
      it('should have a updateLoggingResource function', (done) => {
        try {
          assert.equal(true, typeof a.updateLoggingResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggingResourceId', (done) => {
        try {
          a.updateLoggingResource(null, null, (data, error) => {
            try {
              const displayE = 'loggingResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateLoggingResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLoggingResource - errors', () => {
      it('should have a deleteLoggingResource function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLoggingResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggingResourceId', (done) => {
        try {
          a.deleteLoggingResource(null, (data, error) => {
            try {
              const displayE = 'loggingResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteLoggingResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFirewallLogs - errors', () => {
      it('should have a listFirewallLogs function', (done) => {
        try {
          assert.equal(true, typeof a.listFirewallLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggingResourceId', (done) => {
        try {
          a.listFirewallLogs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loggingResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listFirewallLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFirewallLog - errors', () => {
      it('should have a createFirewallLog function', (done) => {
        try {
          assert.equal(true, typeof a.createFirewallLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggingResourceId', (done) => {
        try {
          a.createFirewallLog(null, null, (data, error) => {
            try {
              const displayE = 'loggingResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createFirewallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showFirewallLogDetails - errors', () => {
      it('should have a showFirewallLogDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showFirewallLogDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggingResourceId', (done) => {
        try {
          a.showFirewallLogDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'loggingResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showFirewallLogDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallLogId', (done) => {
        try {
          a.showFirewallLogDetails('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'firewallLogId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showFirewallLogDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFirewallLog - errors', () => {
      it('should have a updateFirewallLog function', (done) => {
        try {
          assert.equal(true, typeof a.updateFirewallLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggingResourceId', (done) => {
        try {
          a.updateFirewallLog(null, null, null, (data, error) => {
            try {
              const displayE = 'loggingResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateFirewallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallLogId', (done) => {
        try {
          a.updateFirewallLog('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'firewallLogId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateFirewallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFirewallLog - errors', () => {
      it('should have a deleteFirewallLog function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFirewallLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggingResourceId', (done) => {
        try {
          a.deleteFirewallLog(null, null, (data, error) => {
            try {
              const displayE = 'loggingResourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteFirewallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firewallLogId', (done) => {
        try {
          a.deleteFirewallLog('fakeparam', null, (data, error) => {
            try {
              const displayE = 'firewallLogId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteFirewallLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBGPVPNs - errors', () => {
      it('should have a listBGPVPNs function', (done) => {
        try {
          assert.equal(true, typeof a.listBGPVPNs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBGPVPNS - errors', () => {
      it('should have a createBGPVPNS function', (done) => {
        try {
          assert.equal(true, typeof a.createBGPVPNS === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showBGPVPNDetails - errors', () => {
      it('should have a showBGPVPNDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showBGPVPNDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.showBGPVPNDetails(null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showBGPVPNDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateABGPVPN - errors', () => {
      it('should have a updateABGPVPN function', (done) => {
        try {
          assert.equal(true, typeof a.updateABGPVPN === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.updateABGPVPN(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateABGPVPN', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBGPVPN - errors', () => {
      it('should have a deleteBGPVPN function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBGPVPN === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.deleteBGPVPN(null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteBGPVPN', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkAssociations - errors', () => {
      it('should have a listNetworkAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.listNetworkAssociations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listNetworkAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkAssociation - errors', () => {
      it('should have a createNetworkAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.createNetworkAssociation(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createNetworkAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showNetworkAssociationDetails - errors', () => {
      it('should have a showNetworkAssociationDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showNetworkAssociationDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.showNetworkAssociationDetails(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showNetworkAssociationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAssociationId', (done) => {
        try {
          a.showNetworkAssociationDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkAssociationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showNetworkAssociationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkAssociation - errors', () => {
      it('should have a deleteNetworkAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.deleteNetworkAssociation(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteNetworkAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAssociationId', (done) => {
        try {
          a.deleteNetworkAssociation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkAssociationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteNetworkAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRouterAssociations - errors', () => {
      it('should have a listRouterAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.listRouterAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.listRouterAssociations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listRouterAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRouterAssociation - errors', () => {
      it('should have a createRouterAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.createRouterAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.createRouterAssociation(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createRouterAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showRouterAssociationDetails - errors', () => {
      it('should have a showRouterAssociationDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showRouterAssociationDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.showRouterAssociationDetails(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showRouterAssociationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerAssociationId', (done) => {
        try {
          a.showRouterAssociationDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routerAssociationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showRouterAssociationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateARouterAssociationBgpvpnRoutesControlExtension - errors', () => {
      it('should have a updateARouterAssociationBgpvpnRoutesControlExtension function', (done) => {
        try {
          assert.equal(true, typeof a.updateARouterAssociationBgpvpnRoutesControlExtension === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.updateARouterAssociationBgpvpnRoutesControlExtension(null, null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateARouterAssociationBgpvpnRoutesControlExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerAssociationId', (done) => {
        try {
          a.updateARouterAssociationBgpvpnRoutesControlExtension('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routerAssociationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateARouterAssociationBgpvpnRoutesControlExtension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouterAssociation - errors', () => {
      it('should have a deleteRouterAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRouterAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.deleteRouterAssociation(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteRouterAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerAssociationId', (done) => {
        try {
          a.deleteRouterAssociation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routerAssociationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteRouterAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPortAssociations - errors', () => {
      it('should have a listPortAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.listPortAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.listPortAssociations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listPortAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPortAssociation - errors', () => {
      it('should have a createPortAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.createPortAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.createPortAssociation(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createPortAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showPortAssociationDetails - errors', () => {
      it('should have a showPortAssociationDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showPortAssociationDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.showPortAssociationDetails(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showPortAssociationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portAssociationId', (done) => {
        try {
          a.showPortAssociationDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portAssociationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showPortAssociationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAPortAssociation - errors', () => {
      it('should have a updateAPortAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.updateAPortAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.updateAPortAssociation(null, null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAPortAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portAssociationId', (done) => {
        try {
          a.updateAPortAssociation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'portAssociationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAPortAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortAssociation - errors', () => {
      it('should have a deletePortAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.deletePortAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpvpnId', (done) => {
        try {
          a.deletePortAssociation(null, null, (data, error) => {
            try {
              const displayE = 'bgpvpnId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deletePortAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portAssociationId', (done) => {
        try {
          a.deletePortAssociation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portAssociationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deletePortAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBGPSpeakers - errors', () => {
      it('should have a listBGPSpeakers function', (done) => {
        try {
          assert.equal(true, typeof a.listBGPSpeakers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBGPSpeaker - errors', () => {
      it('should have a createBGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.createBGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showBGPSpeakerDetails - errors', () => {
      it('should have a showBGPSpeakerDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showBGPSpeakerDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpSpeakerId', (done) => {
        try {
          a.showBGPSpeakerDetails(null, (data, error) => {
            try {
              const displayE = 'bgpSpeakerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showBGPSpeakerDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateABGPSpeaker - errors', () => {
      it('should have a updateABGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.updateABGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpSpeakerId', (done) => {
        try {
          a.updateABGPSpeaker(null, null, (data, error) => {
            try {
              const displayE = 'bgpSpeakerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateABGPSpeaker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteABGPSpeaker - errors', () => {
      it('should have a deleteABGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.deleteABGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpSpeakerId', (done) => {
        try {
          a.deleteABGPSpeaker(null, (data, error) => {
            try {
              const displayE = 'bgpSpeakerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteABGPSpeaker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBGPPeerToABGPSpeaker - errors', () => {
      it('should have a addBGPPeerToABGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.addBGPPeerToABGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpSpeakerId', (done) => {
        try {
          a.addBGPPeerToABGPSpeaker(null, null, (data, error) => {
            try {
              const displayE = 'bgpSpeakerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addBGPPeerToABGPSpeaker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeBGPPeerFromABGPSpeaker - errors', () => {
      it('should have a removeBGPPeerFromABGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.removeBGPPeerFromABGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpSpeakerId', (done) => {
        try {
          a.removeBGPPeerFromABGPSpeaker(null, null, (data, error) => {
            try {
              const displayE = 'bgpSpeakerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeBGPPeerFromABGPSpeaker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkToABGPSpeaker - errors', () => {
      it('should have a addNetworkToABGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.addNetworkToABGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpSpeakerId', (done) => {
        try {
          a.addNetworkToABGPSpeaker(null, null, (data, error) => {
            try {
              const displayE = 'bgpSpeakerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-addNetworkToABGPSpeaker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkFromABGPSpeaker - errors', () => {
      it('should have a deleteNetworkFromABGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkFromABGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRoutesAdvertisedByABGPSpeaker - errors', () => {
      it('should have a listRoutesAdvertisedByABGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.listRoutesAdvertisedByABGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDynamicRoutingAgentsHostingASpecificBGPSpeaker - errors', () => {
      it('should have a listDynamicRoutingAgentsHostingASpecificBGPSpeaker function', (done) => {
        try {
          assert.equal(true, typeof a.listDynamicRoutingAgentsHostingASpecificBGPSpeaker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBGPPeers - errors', () => {
      it('should have a listBGPPeers function', (done) => {
        try {
          assert.equal(true, typeof a.listBGPPeers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createABGPPeer - errors', () => {
      it('should have a createABGPPeer function', (done) => {
        try {
          assert.equal(true, typeof a.createABGPPeer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showBGPPeerDetails - errors', () => {
      it('should have a showBGPPeerDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showBGPPeerDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpPeerId', (done) => {
        try {
          a.showBGPPeerDetails(null, (data, error) => {
            try {
              const displayE = 'bgpPeerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showBGPPeerDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateABGPPeer - errors', () => {
      it('should have a updateABGPPeer function', (done) => {
        try {
          assert.equal(true, typeof a.updateABGPPeer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpPeerId', (done) => {
        try {
          a.updateABGPPeer(null, null, (data, error) => {
            try {
              const displayE = 'bgpPeerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateABGPPeer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteABGPPeer - errors', () => {
      it('should have a deleteABGPPeer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteABGPPeer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpPeerId', (done) => {
        try {
          a.deleteABGPPeer(null, (data, error) => {
            try {
              const displayE = 'bgpPeerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteABGPPeer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLogs - errors', () => {
      it('should have a listLogs function', (done) => {
        try {
          assert.equal(true, typeof a.listLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLog - errors', () => {
      it('should have a createLog function', (done) => {
        try {
          assert.equal(true, typeof a.createLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showLog - errors', () => {
      it('should have a showLog function', (done) => {
        try {
          assert.equal(true, typeof a.showLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logId', (done) => {
        try {
          a.showLog(null, (data, error) => {
            try {
              const displayE = 'logId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLog - errors', () => {
      it('should have a updateLog function', (done) => {
        try {
          assert.equal(true, typeof a.updateLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logId', (done) => {
        try {
          a.updateLog(null, null, (data, error) => {
            try {
              const displayE = 'logId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLog - errors', () => {
      it('should have a deleteLog function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logId', (done) => {
        try {
          a.deleteLog(null, (data, error) => {
            try {
              const displayE = 'logId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteLog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLoggableResources - errors', () => {
      it('should have a listLoggableResources function', (done) => {
        try {
          assert.equal(true, typeof a.listLoggableResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllAgents - errors', () => {
      it('should have a listAllAgents function', (done) => {
        try {
          assert.equal(true, typeof a.listAllAgents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAgentDetails - errors', () => {
      it('should have a showAgentDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showAgentDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.showAgentDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showAgentDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAgent - errors', () => {
      it('should have a updateAgent function', (done) => {
        try {
          assert.equal(true, typeof a.updateAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.updateAgent(null, null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAgent - errors', () => {
      it('should have a deleteAgent function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.deleteAgent(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllAvailabilityZones - errors', () => {
      it('should have a listAllAvailabilityZones function', (done) => {
        try {
          assert.equal(true, typeof a.listAllAvailabilityZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRoutersHostedByAnL3Agent - errors', () => {
      it('should have a listRoutersHostedByAnL3Agent function', (done) => {
        try {
          assert.equal(true, typeof a.listRoutersHostedByAnL3Agent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.listRoutersHostedByAnL3Agent(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listRoutersHostedByAnL3Agent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduleRouterToAnL3Agent - errors', () => {
      it('should have a scheduleRouterToAnL3Agent function', (done) => {
        try {
          assert.equal(true, typeof a.scheduleRouterToAnL3Agent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.scheduleRouterToAnL3Agent(null, null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-scheduleRouterToAnL3Agent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeL3RouterFromAnL3Agent - errors', () => {
      it('should have a removeL3RouterFromAnL3Agent function', (done) => {
        try {
          assert.equal(true, typeof a.removeL3RouterFromAnL3Agent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.removeL3RouterFromAnL3Agent(null, null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeL3RouterFromAnL3Agent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.removeL3RouterFromAnL3Agent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeL3RouterFromAnL3Agent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listL3AgentsHostingARouter - errors', () => {
      it('should have a listL3AgentsHostingARouter function', (done) => {
        try {
          assert.equal(true, typeof a.listL3AgentsHostingARouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.listL3AgentsHostingARouter(null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listL3AgentsHostingARouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworksHostedByADHCPAgent - errors', () => {
      it('should have a listNetworksHostedByADHCPAgent function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworksHostedByADHCPAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.listNetworksHostedByADHCPAgent(null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listNetworksHostedByADHCPAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduleANetworkToADHCPAgent - errors', () => {
      it('should have a scheduleANetworkToADHCPAgent function', (done) => {
        try {
          assert.equal(true, typeof a.scheduleANetworkToADHCPAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.scheduleANetworkToADHCPAgent(null, null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-scheduleANetworkToADHCPAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeNetworkFromADHCPAgent - errors', () => {
      it('should have a removeNetworkFromADHCPAgent function', (done) => {
        try {
          assert.equal(true, typeof a.removeNetworkFromADHCPAgent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agentId', (done) => {
        try {
          a.removeNetworkFromADHCPAgent(null, null, (data, error) => {
            try {
              const displayE = 'agentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeNetworkFromADHCPAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.removeNetworkFromADHCPAgent('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-removeNetworkFromADHCPAgent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDHCPAgentsHostingANetwork - errors', () => {
      it('should have a listDHCPAgentsHostingANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listDHCPAgentsHostingANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listDHCPAgentsHostingANetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-listDHCPAgentsHostingANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAutoAllocatedTopologyDetails - errors', () => {
      it('should have a showAutoAllocatedTopologyDetails function', (done) => {
        try {
          assert.equal(true, typeof a.showAutoAllocatedTopologyDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.showAutoAllocatedTopologyDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showAutoAllocatedTopologyDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTheAutoAllocatedTopology - errors', () => {
      it('should have a deleteTheAutoAllocatedTopology function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTheAutoAllocatedTopology === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteTheAutoAllocatedTopology(null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteTheAutoAllocatedTopology', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTapServices - errors', () => {
      it('should have a listTapServices function', (done) => {
        try {
          assert.equal(true, typeof a.listTapServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTapService - errors', () => {
      it('should have a createTapService function', (done) => {
        try {
          assert.equal(true, typeof a.createTapService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createTapService('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createTapService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTapService - errors', () => {
      it('should have a updateTapService function', (done) => {
        try {
          assert.equal(true, typeof a.updateTapService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateTapService(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateTapService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTapService - errors', () => {
      it('should have a deleteTapService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTapService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteTapService(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteTapService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showTapService - errors', () => {
      it('should have a showTapService function', (done) => {
        try {
          assert.equal(true, typeof a.showTapService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.showTapService('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showTapService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTapFlow - errors', () => {
      it('should have a listTapFlow function', (done) => {
        try {
          assert.equal(true, typeof a.listTapFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTapFlow - errors', () => {
      it('should have a createTapFlow function', (done) => {
        try {
          assert.equal(true, typeof a.createTapFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createTapFlow('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-createTapFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTapFlow - errors', () => {
      it('should have a updateTapFlow function', (done) => {
        try {
          assert.equal(true, typeof a.updateTapFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateTapFlow(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-updateTapFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTapFlow - errors', () => {
      it('should have a deleteTapFlow function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTapFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteTapFlow('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-deleteTapFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showTapFlow - errors', () => {
      it('should have a showTapFlow function', (done) => {
        try {
          assert.equal(true, typeof a.showTapFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.showTapFlow('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-openstack_neutron-adapter-showTapFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
