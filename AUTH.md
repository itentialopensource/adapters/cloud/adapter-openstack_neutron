## Authenticating OpenStack Neutron Adapter 

This document will go through the steps for authenticating the OpenStack Neutron adapter with Two Step Token from Keystone. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>. 

### Two Step Token
The OpenStack Neutron adapter requires Authentication Token from Keystone. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a OpenStack Neutron server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. Use the properties below for the ```properties.authentication``` field
```json
"authentication": {
  "auth_method": "request_token",
  "username": "<username>",
  "password": "<password>",
  "token": "",
  "token_timeout": 600000,
  "token_cache": "local",
  "invalid_token_error": 401,
  "auth_field": "header.headers.X-Auth-Token",
  "auth_field_format": "{token}",
  "auth_logging": false,
  "os_user_domain_name": "Default",
  "os_project_name": "admin",
  "os_project_domain_name": "Default",
  "sso": {
        "protocol": "https",
        "host": "<host>",
        "port": 5000
  }
}
```
4. `sso` can be configured with keystone auth data in the authentication section of the adapter config as shown above. Optionally, it can also be configured in .system/action.json.

you can leave all of the other properties in the authentication section, they will not be used when the auth_method is request_token.

5. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct credentials.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Credentials should be ** masked ** by the adapter so make sure you verify the username and password - including that there are erroneous spaces at the front or end.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
