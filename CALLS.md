## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Openstack Neutron. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Openstack Neutron.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Openstack_neutron. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listAPIVersions(callback)</td>
    <td style="padding:15px">List API versions</td>
    <td style="padding:15px">v0:{base_path}/{version}/?{query} <br /> v1:{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAPIV2Details(callback)</td>
    <td style="padding:15px">Show API v2 details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/?{query} <br /> v1:{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensions(callback)</td>
    <td style="padding:15px">List extensions</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/extensions?{query} <br /> v1:{base_path}/{version}/extensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showExtensionDetails(alias, callback)</td>
    <td style="padding:15px">Show extension details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/extensions/{pathv1}?{query} <br /> v1:{base_path}/{version}/extensions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showNetworkDetails(fields, networkId, callback)</td>
    <td style="padding:15px">Show network details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/networks/{pathv1}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update network</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/networks/{pathv1}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetwork(networkId, callback)</td>
    <td style="padding:15px">Delete network</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/networks/{pathv1}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworks(adminStateUp, id, mtu, name, projectId, providerNetworkType, providerPhysicalNetwork, providerSegmentationId, revisionNumber, routerExternal, shared, status, tenantId, vlanTransparent, description, isDefault, tags, tagsAny, notTags, notTagsAny, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List networks</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/networks?{query} <br /> v1:{base_path}/{version}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetwork(body, callback)</td>
    <td style="padding:15px">Create network</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/networks?{query} <br /> v1:{base_path}/{version}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showNetworkSegmentRangeDetails(networkSegmentRangeId, callback)</td>
    <td style="padding:15px">Show network segment range details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/network_segment_ranges/{pathv1}?{query} <br /> v1:{base_path}/{version}/network_segment_ranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSegmentRange(tags, tagsAny, notTags, notTagsAny, networkSegmentRangeId, body, callback)</td>
    <td style="padding:15px">Update network segment range</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/network_segment_ranges/{pathv1}?{query} <br /> v1:{base_path}/{version}/network_segment_ranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSegmentRange(networkSegmentRangeId, callback)</td>
    <td style="padding:15px">Delete network segment range</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/network_segment_ranges/{pathv1}?{query} <br /> v1:{base_path}/{version}/network_segment_ranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkSegmentRanges(id, name, tenantId, projectId, networkType, physicalNetwork, sortDir, sortKey, tags, tagsAny, notTags, notTagsAny, fields, callback)</td>
    <td style="padding:15px">List network segment ranges</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/network_segment_ranges?{query} <br /> v1:{base_path}/{version}/network_segment_ranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSegmentRange(body, callback)</td>
    <td style="padding:15px">Create network segment range</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/network_segment_ranges?{query} <br /> v1:{base_path}/{version}/network_segment_ranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPortDetails(fields, portId, callback)</td>
    <td style="padding:15px">Show port details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ports/{pathv1}?{query} <br /> v1:{base_path}/{version}/ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePort(portId, body, callback)</td>
    <td style="padding:15px">Update port</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ports/{pathv1}?{query} <br /> v1:{base_path}/{version}/ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePort(portId, callback)</td>
    <td style="padding:15px">Delete port</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ports/{pathv1}?{query} <br /> v1:{base_path}/{version}/ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPorts(adminStateUp, bindingHostId, description, deviceId, deviceOwner, fixedIps, id, ipAllocation, macAddress, name, networkId, projectId, revisionNumber, sortDir, sortKey, status, tenantId, tags, tagsAny, notTags, notTagsAny, fields, macLearningEnabled, callback)</td>
    <td style="padding:15px">List ports</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ports?{query} <br /> v1:{base_path}/{version}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPort(body, callback)</td>
    <td style="padding:15px">Create port</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ports?{query} <br /> v1:{base_path}/{version}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showSegmentDetails(segmentId, callback)</td>
    <td style="padding:15px">Show segment details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/segments/{pathv1}?{query} <br /> v1:{base_path}/{version}/segments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSegment(segmentId, body, callback)</td>
    <td style="padding:15px">Update segment</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/segments/{pathv1}?{query} <br /> v1:{base_path}/{version}/segments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSegment(segmentId, callback)</td>
    <td style="padding:15px">Delete segment</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/segments/{pathv1}?{query} <br /> v1:{base_path}/{version}/segments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSegments(id, networkId, physicalNetwork, networkType, revisionNumber, segmentationId, name, description, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List segments</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/segments?{query} <br /> v1:{base_path}/{version}/segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSegment(body, callback)</td>
    <td style="padding:15px">Create segment</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/segments?{query} <br /> v1:{base_path}/{version}/segments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTrunks(adminStateUp, description, id, name, portId, revisionNumber, status, tenantId, projectId, sortDir, sortKey, tags, tagsAny, notTags, notTagsAny, callback)</td>
    <td style="padding:15px">List trunks</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/trunks?{query} <br /> v1:{base_path}/{version}/trunks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTrunk(body, callback)</td>
    <td style="padding:15px">Create trunk</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/trunks?{query} <br /> v1:{base_path}/{version}/trunks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSubportsToTrunk(trunkId, body, callback)</td>
    <td style="padding:15px">Add subports to trunk</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/trunks/{pathv1}/add_subports?{query} <br /> v1:{base_path}/{version}/trunks/{pathv1}/add_subports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubportsFromTrunk(trunkId, body, callback)</td>
    <td style="padding:15px">Delete subports from trunk</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/trunks/{pathv1}/remove_subports?{query} <br /> v1:{base_path}/{version}/trunks/{pathv1}/remove_subports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSubportsForTrunk(trunkId, callback)</td>
    <td style="padding:15px">List subports for trunk</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/trunks/{pathv1}/get_subports?{query} <br /> v1:{base_path}/{version}/trunks/{pathv1}/get_subports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTrunk(trunkId, body, callback)</td>
    <td style="padding:15px">Update trunk</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/trunks/{pathv1}?{query} <br /> v1:{base_path}/{version}/trunks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showTrunk(trunkId, callback)</td>
    <td style="padding:15px">Show trunk</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/trunks/{pathv1}?{query} <br /> v1:{base_path}/{version}/trunks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTrunk(trunkId, callback)</td>
    <td style="padding:15px">Delete trunk</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/trunks/{pathv1}?{query} <br /> v1:{base_path}/{version}/trunks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAddressScope(fields, addressScopeId, callback)</td>
    <td style="padding:15px">Show address scope</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-scopes/{pathv1}?{query} <br /> v1:{base_path}/{version}/address-scopes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnAddressScope(addressScopeId, body, callback)</td>
    <td style="padding:15px">Update an address scope</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-scopes/{pathv1}?{query} <br /> v1:{base_path}/{version}/address-scopes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnAddressScope(addressScopeId, callback)</td>
    <td style="padding:15px">Delete an address scope</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-scopes/{pathv1}?{query} <br /> v1:{base_path}/{version}/address-scopes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAddressScopes(id, name, tenantId, projectId, ipVersion, shared, sortKey, fields, callback)</td>
    <td style="padding:15px">List address scopes</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-scopes?{query} <br /> v1:{base_path}/{version}/address-scopes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAddressScope(body, callback)</td>
    <td style="padding:15px">Create address scope</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-scopes?{query} <br /> v1:{base_path}/{version}/address-scopes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showConntrackHelper(fields, routerId, conntrackHelperId, callback)</td>
    <td style="padding:15px">Show conntrack helper</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/conntrack_helpers/{pathv2}?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/conntrack_helpers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAConntrackHelper(routerId, conntrackHelperId, body, callback)</td>
    <td style="padding:15px">Update a conntrack helper</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/conntrack_helpers/{pathv2}?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/conntrack_helpers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAConntrackHelper(routerId, conntrackHelperId, callback)</td>
    <td style="padding:15px">Delete a conntrack helper</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/conntrack_helpers/{pathv2}?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/conntrack_helpers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRouterConntrackHelpers(id, helper, protocol, port, sortKey, sortDir, fields, routerId, callback)</td>
    <td style="padding:15px">List router conntrack helpers</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/conntrack_helpers?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/conntrack_helpers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConntrackHelper(routerId, body, callback)</td>
    <td style="padding:15px">Create conntrack helper</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/conntrack_helpers?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/conntrack_helpers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFloatingIPs(id, routerId, status, tenantId, projectId, revisionNumber, description, floatingNetworkId, fixedIpAddress, floatingIpAddress, portId, sortDir, sortKey, tags, tagsAny, notTags, notTagsAny, fields, callback)</td>
    <td style="padding:15px">List floating IPs</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips?{query} <br /> v1:{base_path}/{version}/floatingips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFloatingIP(body, callback)</td>
    <td style="padding:15px">Create floating IP</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips?{query} <br /> v1:{base_path}/{version}/floatingips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFloatingIPDetails(floatingipId, callback)</td>
    <td style="padding:15px">Show floating IP details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips/{pathv1}?{query} <br /> v1:{base_path}/{version}/floatingips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFloatingIP(floatingipId, body, callback)</td>
    <td style="padding:15px">Update floating IP</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips/{pathv1}?{query} <br /> v1:{base_path}/{version}/floatingips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFloatingIP(floatingipId, callback)</td>
    <td style="padding:15px">Delete floating IP</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips/{pathv1}?{query} <br /> v1:{base_path}/{version}/floatingips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFloatingIPPools(callback)</td>
    <td style="padding:15px">List floating IP Pools</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingip_pools?{query} <br /> v1:{base_path}/{version}/floatingip_pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPortForwarding(fields, floatingipId, portForwardingId, callback)</td>
    <td style="padding:15px">Show port forwarding</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips/{pathv1}/port_forwardings/{pathv2}?{query} <br /> v1:{base_path}/{version}/floatingips/{pathv1}/port_forwardings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAPortForwarding(floatingipId, portForwardingId, body, callback)</td>
    <td style="padding:15px">Update a port forwarding</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips/{pathv1}/port_forwardings/{pathv2}?{query} <br /> v1:{base_path}/{version}/floatingips/{pathv1}/port_forwardings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAFloatingIPPortForwarding(floatingipId, portForwardingId, callback)</td>
    <td style="padding:15px">Delete a floating IP port forwarding</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips/{pathv1}/port_forwardings/{pathv2}?{query} <br /> v1:{base_path}/{version}/floatingips/{pathv1}/port_forwardings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFloatingIPPortForwardings(id, internalPortId, externalPort, externalPortRange, protocol, sortKey, sortDir, fields, floatingipId, callback)</td>
    <td style="padding:15px">List floating IP port forwardings</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips/{pathv1}/port_forwardings?{query} <br /> v1:{base_path}/{version}/floatingips/{pathv1}/port_forwardings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortForwarding(floatingipId, body, callback)</td>
    <td style="padding:15px">Create port forwarding</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/floatingips/{pathv1}/port_forwardings?{query} <br /> v1:{base_path}/{version}/floatingips/{pathv1}/port_forwardings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRouters(id, tenantId, projectId, name, description, adminStateUp, revisionNumber, sortDir, sortKey, tags, tagsAny, notTags, notTagsAny, fields, callback)</td>
    <td style="padding:15px">List routers</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers?{query} <br /> v1:{base_path}/{version}/routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouter(body, callback)</td>
    <td style="padding:15px">Create router</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers?{query} <br /> v1:{base_path}/{version}/routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showRouterDetails(fields, routerId, callback)</td>
    <td style="padding:15px">Show router details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRouter(routerId, body, callback)</td>
    <td style="padding:15px">Update router</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouter(routerId, callback)</td>
    <td style="padding:15px">Delete router</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInterfaceToRouter(routerId, body, callback)</td>
    <td style="padding:15px">Add interface to router</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/add_router_interface?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/add_router_interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeInterfaceFromRouter(routerId, body, callback)</td>
    <td style="padding:15px">Remove interface from router</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/remove_router_interface?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/remove_router_interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addExtraRoutesToRouter(routerId, body, callback)</td>
    <td style="padding:15px">Add extra routes to router</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/add_extraroutes?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/add_extraroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeExtraRoutesFromRouter(routerId, body, callback)</td>
    <td style="padding:15px">Remove extra routes from router</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/remove_extraroutes?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/remove_extraroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNdpProxies(id, tenantId, projectId, name, description, routerId, portId, ipAddress, revisionNumber, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List ndp proxies</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ndp_proxies?{query} <br /> v1:{base_path}/{version}/ndp_proxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNdpProxy(body, callback)</td>
    <td style="padding:15px">Create ndp proxy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ndp_proxies?{query} <br /> v1:{base_path}/{version}/ndp_proxies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showNdpProxyDetails(fields, ndpProxyId, callback)</td>
    <td style="padding:15px">Show ndp proxy details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ndp_proxies/{pathv1}?{query} <br /> v1:{base_path}/{version}/ndp_proxies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateANdpProxy(ndpProxyId, body, callback)</td>
    <td style="padding:15px">Update a ndp proxy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ndp_proxies/{pathv1}?{query} <br /> v1:{base_path}/{version}/ndp_proxies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteANdpProxy(ndpProxyId, callback)</td>
    <td style="padding:15px">Delete a ndp proxy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/ndp_proxies/{pathv1}?{query} <br /> v1:{base_path}/{version}/ndp_proxies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showSubnetPool(fields, subnetpoolId, callback)</td>
    <td style="padding:15px">Show subnet pool</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnetpools/{pathv1}?{query} <br /> v1:{base_path}/{version}/subnetpools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSubnetPool(subnetpoolId, body, callback)</td>
    <td style="padding:15px">Update subnet pool</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnetpools/{pathv1}?{query} <br /> v1:{base_path}/{version}/subnetpools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnetPool(subnetpoolId, callback)</td>
    <td style="padding:15px">Delete subnet pool</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnetpools/{pathv1}?{query} <br /> v1:{base_path}/{version}/subnetpools/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSubnetPools(id, name, defaultQuota, tenantId, projectId, minPrefixlen, addressScopeId, ipVersion, shared, defaultPrefixlen, maxPrefixlen, description, isDefault, revisionNumber, sortDir, sortKey, tags, tagsAny, notTags, notTagsAny, fields, callback)</td>
    <td style="padding:15px">List subnet pools</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnetpools?{query} <br /> v1:{base_path}/{version}/subnetpools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubnetPool(body, callback)</td>
    <td style="padding:15px">Create subnet pool</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnetpools?{query} <br /> v1:{base_path}/{version}/subnetpools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPrefixes(subnetpoolId, body, callback)</td>
    <td style="padding:15px">Add prefixes</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnetpools/{pathv1}/add_prefixes?{query} <br /> v1:{base_path}/{version}/subnetpools/{pathv1}/add_prefixes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removePrefixes(subnetpoolId, body, callback)</td>
    <td style="padding:15px">Remove prefixes</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnetpools/{pathv1}/remove_prefixes?{query} <br /> v1:{base_path}/{version}/subnetpools/{pathv1}/remove_prefixes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSubnets(id, tenantId, projectId, name, enableDhcp, networkId, ipVersion, gatewayIp, cidr, description, ipv6AddressMode, ipv6RaMode, revisionNumber, segmentId, shared, sortDir, sortKey, subnetpoolId, tags, tagsAny, notTags, notTagsAny, dnsPublishFixedIp, fields, callback)</td>
    <td style="padding:15px">List subnets</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnets?{query} <br /> v1:{base_path}/{version}/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubnet(body, callback)</td>
    <td style="padding:15px">Create subnet</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnets?{query} <br /> v1:{base_path}/{version}/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showSubnetDetails(subnetId, callback)</td>
    <td style="padding:15px">Show subnet details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnets/{pathv1}?{query} <br /> v1:{base_path}/{version}/subnets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSubnet(subnetId, body, callback)</td>
    <td style="padding:15px">Update subnet</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnets/{pathv1}?{query} <br /> v1:{base_path}/{version}/subnets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnet(subnetId, callback)</td>
    <td style="padding:15px">Delete subnet</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/subnets/{pathv1}?{query} <br /> v1:{base_path}/{version}/subnets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLocalIPs(id, name, description, projectId, localPortId, networkId, localIpAddress, ipMode, revisionNumber, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List Local IPs</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/local_ips?{query} <br /> v1:{base_path}/{version}/local_ips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLocalIP(body, callback)</td>
    <td style="padding:15px">Create Local IP</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/local_ips?{query} <br /> v1:{base_path}/{version}/local_ips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showLocalIPDetails(fields, localIpId, callback)</td>
    <td style="padding:15px">Show Local IP details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/local_ips/{pathv1}?{query} <br /> v1:{base_path}/{version}/local_ips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLocalIP(localIpId, body, callback)</td>
    <td style="padding:15px">Update Local IP</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/local_ips/{pathv1}?{query} <br /> v1:{base_path}/{version}/local_ips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLocalIP(localIpId, callback)</td>
    <td style="padding:15px">Delete Local IP</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/local_ips/{pathv1}?{query} <br /> v1:{base_path}/{version}/local_ips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLocalIPAssociations(fixedPortId, fixedIp, host, sortDir, sortKey, fields, localIpId, callback)</td>
    <td style="padding:15px">List Local IP Associations</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/local_ips/{pathv1}/port_associations?{query} <br /> v1:{base_path}/{version}/local_ips/{pathv1}/port_associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLocalIPAssociation(localIpId, body, callback)</td>
    <td style="padding:15px">Create Local IP Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/local_ips/{pathv1}/port_associations?{query} <br /> v1:{base_path}/{version}/local_ips/{pathv1}/port_associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLocalIPAssociation(localIpId, fixedPortId, callback)</td>
    <td style="padding:15px">Delete Local IP Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/local_ips/{pathv1}/port_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/local_ips/{pathv1}/port_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAddressGroup(fields, addressGroupId, callback)</td>
    <td style="padding:15px">Show address group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/address-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAddressGroup(addressGroupId, body, callback)</td>
    <td style="padding:15px">Create address group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/address-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnAddressGroup(addressGroupId, body, callback)</td>
    <td style="padding:15px">Update an address group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/address-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnAddressGroup(addressGroupId, callback)</td>
    <td style="padding:15px">Delete an address group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/address-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAddressGroups(id, name, tenantId, projectId, sortKey, fields, callback)</td>
    <td style="padding:15px">List address groups</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-groups?{query} <br /> v1:{base_path}/{version}/address-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIPAddressesToAddressGroup(addressGroupId, body, callback)</td>
    <td style="padding:15px">Add IP addresses to address group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-group/{pathv1}/add_addresses?{query} <br /> v1:{base_path}/{version}/address-group/{pathv1}/add_addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIPAddressesToAddressGroup(addressGroupId, body, callback)</td>
    <td style="padding:15px">Remove IP addresses to address group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/address-group/{pathv1}/remove_addresses?{query} <br /> v1:{base_path}/{version}/address-group/{pathv1}/remove_addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFirewallGroups(fields, callback)</td>
    <td style="padding:15px">List firewall groups</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_groups?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFirewallGroup(body, callback)</td>
    <td style="padding:15px">Create firewall group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_groups?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFirewallGroupDetails(firewallGroupId, callback)</td>
    <td style="padding:15px">Show firewall group details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFirewallGroup(firewallGroupId, body, callback)</td>
    <td style="padding:15px">Update firewall group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallGroup(firewallGroupId, callback)</td>
    <td style="padding:15px">Delete firewall group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFirewallPolicies(fields, callback)</td>
    <td style="padding:15px">List firewall policies</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_policies?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFirewallPolicy(body, callback)</td>
    <td style="padding:15px">Create firewall policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_policies?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFirewallPolicyDetails(firewallPolicyId, callback)</td>
    <td style="padding:15px">Show firewall policy details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFirewallPolicy(firewallPolicyId, body, callback)</td>
    <td style="padding:15px">Update firewall policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallPolicy(firewallPolicyId, callback)</td>
    <td style="padding:15px">Delete firewall policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFirewallRules(fields, callback)</td>
    <td style="padding:15px">List firewall rules</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_rules?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFirewallRule(body, callback)</td>
    <td style="padding:15px">Create firewall rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_rules?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFirewallRuleDetails(firewallRuleId, callback)</td>
    <td style="padding:15px">Show firewall rule details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFirewallRule(firewallRuleId, body, callback)</td>
    <td style="padding:15px">Update firewall rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallRule(firewallRuleId, callback)</td>
    <td style="padding:15px">Delete firewall rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">insertRuleIntoAFirewallPolicy(firewallPolicyId, body, callback)</td>
    <td style="padding:15px">Insert rule into a firewall policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_policies/{pathv1}/insert_rule?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_policies/{pathv1}/insert_rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeRuleFromFirewallPolicy(firewallPolicyId, body, callback)</td>
    <td style="padding:15px">Remove rule from firewall policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/fwaas/firewall_policies/{pathv1}/remove_rule?{query} <br /> v1:{base_path}/{version}/fwaas/firewall_policies/{pathv1}/remove_rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showRBACPolicyDetails(rbacPolicyId, callback)</td>
    <td style="padding:15px">Show RBAC policy details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/rbac-policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/rbac-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRBACPolicy(rbacPolicyId, body, callback)</td>
    <td style="padding:15px">Update RBAC policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/rbac-policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/rbac-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRBACPolicy(rbacPolicyId, callback)</td>
    <td style="padding:15px">Delete RBAC policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/rbac-policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/rbac-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRBACPolicies(targetTenant, tenantId, objectType, objectId, action, projectId, id, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List RBAC policies</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/rbac-policies?{query} <br /> v1:{base_path}/{version}/rbac-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRBACPolicy(body, callback)</td>
    <td style="padding:15px">Create RBAC policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/rbac-policies?{query} <br /> v1:{base_path}/{version}/rbac-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSecurityGroupRules(remoteGroupId, direction, protocol, ethertype, portRangeMax, securityGroupId, tenantId, projectId, portRangeMin, remoteIpPrefix, revisionNumber, id, description, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List security group rules</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-group-rules?{query} <br /> v1:{base_path}/{version}/security-group-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityGroupRule(body, callback)</td>
    <td style="padding:15px">Create security group rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-group-rules?{query} <br /> v1:{base_path}/{version}/security-group-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showSecurityGroupRule(verbose, fields, securityGroupRuleId, callback)</td>
    <td style="padding:15px">Show security group rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-group-rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/security-group-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityGroupRule(securityGroupRuleId, callback)</td>
    <td style="padding:15px">Delete security group rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-group-rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/security-group-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSecurityGroups(id, tenantId, projectId, revisionNumber, name, description, sortDir, sortKey, shared, tags, tagsAny, notTags, notTagsAny, fields, callback)</td>
    <td style="padding:15px">List security groups</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-groups?{query} <br /> v1:{base_path}/{version}/security-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityGroup(body, callback)</td>
    <td style="padding:15px">Create security group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-groups?{query} <br /> v1:{base_path}/{version}/security-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showSecurityGroup(verbose, fields, securityGroupId, callback)</td>
    <td style="padding:15px">Show security group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/security-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityGroup(securityGroupId, body, callback)</td>
    <td style="padding:15px">Update security group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/security-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityGroup(securityGroupId, callback)</td>
    <td style="padding:15px">Delete security group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/security-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/security-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIKEPolicies(fields, callback)</td>
    <td style="padding:15px">List IKE policies</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ikepolicies?{query} <br /> v1:{base_path}/{version}/vpn/ikepolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIKEPolicy(body, callback)</td>
    <td style="padding:15px">Create IKE policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ikepolicies?{query} <br /> v1:{base_path}/{version}/vpn/ikepolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showIKEPolicyDetails(ikepolicyId, callback)</td>
    <td style="padding:15px">Show IKE policy details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ikepolicies/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ikepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIKEPolicy(ikepolicyId, body, callback)</td>
    <td style="padding:15px">Update IKE policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ikepolicies/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ikepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIKEPolicy(ikepolicyId, callback)</td>
    <td style="padding:15px">Remove IKE policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ikepolicies/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ikepolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIPsecPolicies(fields, callback)</td>
    <td style="padding:15px">List IPsec policies</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsecpolicies?{query} <br /> v1:{base_path}/{version}/vpn/ipsecpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIPsecPolicy(body, callback)</td>
    <td style="padding:15px">Create IPsec policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsecpolicies?{query} <br /> v1:{base_path}/{version}/vpn/ipsecpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showIPsecPolicy(ipsecpolicyId, callback)</td>
    <td style="padding:15px">Show IPsec policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsecpolicies/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ipsecpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIPsecPolicy(ipsecpolicyId, body, callback)</td>
    <td style="padding:15px">Update IPsec policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsecpolicies/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ipsecpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIPsecPolicy(ipsecpolicyId, callback)</td>
    <td style="padding:15px">Remove IPsec policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsecpolicies/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ipsecpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIPsecConnections(fields, callback)</td>
    <td style="padding:15px">List IPsec connections</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsec-site-connections?{query} <br /> v1:{base_path}/{version}/vpn/ipsec-site-connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIPsecConnection(body, callback)</td>
    <td style="padding:15px">Create IPsec connection</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsec-site-connections?{query} <br /> v1:{base_path}/{version}/vpn/ipsec-site-connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showIPsecConnection(connectionId, callback)</td>
    <td style="padding:15px">Show IPsec connection</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsec-site-connections/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ipsec-site-connections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIPsecConnection(connectionId, body, callback)</td>
    <td style="padding:15px">Update IPsec connection</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsec-site-connections/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ipsec-site-connections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIPsecConnection(connectionId, callback)</td>
    <td style="padding:15px">Remove IPsec connection</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/ipsec-site-connections/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/ipsec-site-connections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPNEndpointGroups(fields, callback)</td>
    <td style="padding:15px">List VPN endpoint groups</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/endpoint-groups?{query} <br /> v1:{base_path}/{version}/vpn/endpoint-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVPNEndpointGroup(body, callback)</td>
    <td style="padding:15px">Create VPN endpoint group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/endpoint-groups?{query} <br /> v1:{base_path}/{version}/vpn/endpoint-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVPNEndpointGroup(endpointGroupId, callback)</td>
    <td style="padding:15px">Show VPN endpoint group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/endpoint-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/endpoint-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVPNEndpointGroup(endpointGroupId, body, callback)</td>
    <td style="padding:15px">Update VPN endpoint group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/endpoint-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/endpoint-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeVPNEndpointGroup(endpointGroupId, callback)</td>
    <td style="padding:15px">Remove VPN endpoint group</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/endpoint-groups/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/endpoint-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVPNServices(fields, callback)</td>
    <td style="padding:15px">List VPN services</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/vpnservices?{query} <br /> v1:{base_path}/{version}/vpn/vpnservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVPNService(body, callback)</td>
    <td style="padding:15px">Create VPN service</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/vpnservices?{query} <br /> v1:{base_path}/{version}/vpn/vpnservices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVPNServiceDetails(serviceId, callback)</td>
    <td style="padding:15px">Show VPN service details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/vpnservices/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/vpnservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVPNService(serviceId, body, callback)</td>
    <td style="padding:15px">Update VPN service</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/vpnservices/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/vpnservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeVPNService(serviceId, callback)</td>
    <td style="padding:15px">Remove VPN service</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/vpn/vpnservices/{pathv1}?{query} <br /> v1:{base_path}/{version}/vpn/vpnservices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFlavors(id, serviceType, name, description, enabled, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List flavors</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/flavors?{query} <br /> v1:{base_path}/{version}/flavors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFlavor(body, callback)</td>
    <td style="padding:15px">Create flavor</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/flavors?{query} <br /> v1:{base_path}/{version}/flavors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFlavorDetails(flavorId, callback)</td>
    <td style="padding:15px">Show flavor details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/flavors/{pathv1}?{query} <br /> v1:{base_path}/{version}/flavors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlavor(flavorId, body, callback)</td>
    <td style="padding:15px">Update flavor</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/flavors/{pathv1}?{query} <br /> v1:{base_path}/{version}/flavors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFlavor(flavorId, callback)</td>
    <td style="padding:15px">Delete flavor</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/flavors/{pathv1}?{query} <br /> v1:{base_path}/{version}/flavors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateFlavorWithAServiceProfile(flavorId, body, callback)</td>
    <td style="padding:15px">Associate flavor with a service profile</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/flavors/{pathv1}/service_profiles?{query} <br /> v1:{base_path}/{version}/flavors/{pathv1}/service_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateAFlavor(profileId, flavorId, callback)</td>
    <td style="padding:15px">Disassociate a flavor.</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/flavors/{pathv1}/service_profiles/{pathv2}?{query} <br /> v1:{base_path}/{version}/flavors/{pathv1}/service_profiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServiceProfiles(id, enabled, driver, description, sortDir, sortKey, callback)</td>
    <td style="padding:15px">List service profiles</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/service_profiles?{query} <br /> v1:{base_path}/{version}/service_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceProfile(body, callback)</td>
    <td style="padding:15px">Create service profile</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/service_profiles?{query} <br /> v1:{base_path}/{version}/service_profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showServiceProfileDetails(profileId, callback)</td>
    <td style="padding:15px">Show service profile details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/service_profiles/{pathv1}?{query} <br /> v1:{base_path}/{version}/service_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServiceProfile(profileId, body, callback)</td>
    <td style="padding:15px">Update service profile</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/service_profiles/{pathv1}?{query} <br /> v1:{base_path}/{version}/service_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceProfile(profileId, callback)</td>
    <td style="padding:15px">Delete service profile</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/service_profiles/{pathv1}?{query} <br /> v1:{base_path}/{version}/service_profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMeteringLabels(description, tenantId, projectId, shared, id, name, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List metering labels</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/metering/metering-labels?{query} <br /> v1:{base_path}/{version}/metering/metering-labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMeteringLabel(body, callback)</td>
    <td style="padding:15px">Create metering label</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/metering/metering-labels?{query} <br /> v1:{base_path}/{version}/metering/metering-labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMeteringLabelDetails(meteringLabelId, callback)</td>
    <td style="padding:15px">Show metering label details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/metering/metering-labels/{pathv1}?{query} <br /> v1:{base_path}/{version}/metering/metering-labels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMeteringLabel(meteringLabelId, callback)</td>
    <td style="padding:15px">Delete metering label</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/metering/metering-labels/{pathv1}?{query} <br /> v1:{base_path}/{version}/metering/metering-labels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMeteringLabelRules(direction, remoteIpPrefix, sourceIpPrefix, destinationIpPrefix, excluded, meteringLabelId, id, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List metering label rules</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/metering/metering-label-rules?{query} <br /> v1:{base_path}/{version}/metering/metering-label-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMeteringLabelRule(sourceIpPrefix, destinationIpPrefix, body, callback)</td>
    <td style="padding:15px">Create metering label rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/metering/metering-label-rules?{query} <br /> v1:{base_path}/{version}/metering/metering-label-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMeteringLabelRuleDetails(meteringLabelRuleId, callback)</td>
    <td style="padding:15px">Show metering label rule details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/metering/metering-label-rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/metering/metering-label-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMeteringLabelRule(meteringLabelRuleId, callback)</td>
    <td style="padding:15px">Delete metering label rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/metering/metering-label-rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/metering/metering-label-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showNetworkIPAvailability(networkId, callback)</td>
    <td style="padding:15px">Show Network IP Availability</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/network-ip-availabilities/{pathv1}?{query} <br /> v1:{base_path}/{version}/network-ip-availabilities/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkIPAvailability(networkId, networkName, tenantId, projectId, ipVersion, callback)</td>
    <td style="padding:15px">List Network IP Availability</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/network-ip-availabilities?{query} <br /> v1:{base_path}/{version}/network-ip-availabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listQuotasForProjectsWithNonDefaultQuotaValues(callback)</td>
    <td style="padding:15px">List quotas for projects with non-default quota values</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/quotas?{query} <br /> v1:{base_path}/{version}/quotas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listQuotasForAProject(projectId, callback)</td>
    <td style="padding:15px">List quotas for a project</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/quotas/{pathv1}?{query} <br /> v1:{base_path}/{version}/quotas/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQuotaForAProject(projectId, body, callback)</td>
    <td style="padding:15px">Update quota for a project</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/quotas/{pathv1}?{query} <br /> v1:{base_path}/{version}/quotas/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetQuotaForAProject(projectId, callback)</td>
    <td style="padding:15px">Reset quota for a project</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/quotas/{pathv1}?{query} <br /> v1:{base_path}/{version}/quotas/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultQuotasForAProject(projectId, callback)</td>
    <td style="padding:15px">List default quotas for a project</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/quotas/{pathv1}/default?{query} <br /> v1:{base_path}/{version}/quotas/{pathv1}/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showQuotaDetailsForATenant(projectId, callback)</td>
    <td style="padding:15px">Show quota details for a tenant</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/quotas/{pathv1}/details.json?{query} <br /> v1:{base_path}/{version}/quotas/{pathv1}/details.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServiceProviders(fields, callback)</td>
    <td style="padding:15px">List service providers</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/service-providers?{query} <br /> v1:{base_path}/{version}/service-providers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAllTags(resourceType, resourceId, body, callback)</td>
    <td style="padding:15px">Replace all tags</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/{pathv1}/{pathv2}/tags?{query} <br /> v1:{base_path}/{version}/{pathv1}/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAllTags(resourceType, resourceId, callback)</td>
    <td style="padding:15px">Remove all tags</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/{pathv1}/{pathv2}/tags?{query} <br /> v1:{base_path}/{version}/{pathv1}/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">obtainTagList(resourceType, resourceId, callback)</td>
    <td style="padding:15px">Obtain Tag List</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/{pathv1}/{pathv2}/tags?{query} <br /> v1:{base_path}/{version}/{pathv1}/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmATag(resourceType, resourceId, tag, callback)</td>
    <td style="padding:15px">Confirm a tag</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/{pathv1}/{pathv2}/tags/{pathv3}?{query} <br /> v1:{base_path}/{version}/{pathv1}/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addATag(resourceType, resourceId, tag, callback)</td>
    <td style="padding:15px">Add a tag</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/{pathv1}/{pathv2}/tags/{pathv3}?{query} <br /> v1:{base_path}/{version}/{pathv1}/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeATag(resourceType, resourceId, tag, callback)</td>
    <td style="padding:15px">Remove a tag</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/{pathv1}/{pathv2}/tags/{pathv3}?{query} <br /> v1:{base_path}/{version}/{pathv1}/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listQoSRuleTypes(allSupported, allRules, callback)</td>
    <td style="padding:15px">List QoS rule types</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/rule-types?{query} <br /> v1:{base_path}/{version}/qos/rule-types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showQoSRuleTypeDetails(ruleType, callback)</td>
    <td style="padding:15px">Show QoS rule type details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/rule-types/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/rule-types/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listQoSPolicies(description, tenantId, projectId, revisionNumber, shared, id, isDefault, name, tags, tagsAny, notTags, notTagsAny, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List QoS policies</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies?{query} <br /> v1:{base_path}/{version}/qos/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createQoSPolicy(body, callback)</td>
    <td style="padding:15px">Create QoS policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies?{query} <br /> v1:{base_path}/{version}/qos/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showQoSPolicyDetails(policyId, callback)</td>
    <td style="padding:15px">Show QoS policy details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQoSPolicy(policyId, body, callback)</td>
    <td style="padding:15px">Update QoS policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteQoSPolicy(policyId, callback)</td>
    <td style="padding:15px">Delete QoS policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBandwidthLimitRulesForQoSPolicy(maxKbps, id, maxBurstKbps, direction, sortDir, sortKey, policyId, callback)</td>
    <td style="padding:15px">List bandwidth limit rules for QoS policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/bandwidth_limit_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/bandwidth_limit_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBandwidthLimitRule(policyId, body, callback)</td>
    <td style="padding:15px">Create bandwidth limit rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/bandwidth_limit_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/bandwidth_limit_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBandwidthLimitRuleDetails(policyId, ruleId, callback)</td>
    <td style="padding:15px">Show bandwidth limit rule details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/bandwidth_limit_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/bandwidth_limit_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBandwidthLimitRule(policyId, ruleId, body, callback)</td>
    <td style="padding:15px">Update bandwidth limit rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/bandwidth_limit_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/bandwidth_limit_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBandwidthLimitRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Delete bandwidth limit rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/bandwidth_limit_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/bandwidth_limit_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDSCPMarkingRulesForQoSPolicy(dscpMark, id, sortDir, sortKey, policyId, callback)</td>
    <td style="padding:15px">List DSCP marking rules for QoS policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/dscp_marking_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/dscp_marking_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDSCPMarkingRule(policyId, body, callback)</td>
    <td style="padding:15px">Create DSCP marking rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/dscp_marking_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/dscp_marking_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDSCPMarkingRuleDetails(policyId, dscpRuleId, callback)</td>
    <td style="padding:15px">Show DSCP marking rule details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/dscp_marking_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/dscp_marking_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDSCPMarkingRule(policyId, dscpRuleId, body, callback)</td>
    <td style="padding:15px">Update DSCP marking rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/dscp_marking_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/dscp_marking_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDSCPMarkingRule(policyId, dscpRuleId, callback)</td>
    <td style="padding:15px">Delete DSCP marking rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/dscp_marking_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/dscp_marking_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMinimumBandwidthRulesForQoSPolicy(minKbps, id, direction, sortDir, sortKey, policyId, callback)</td>
    <td style="padding:15px">List minimum bandwidth rules for QoS policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_bandwidth_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_bandwidth_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMinimumBandwidthRule(policyId, body, callback)</td>
    <td style="padding:15px">Create minimum bandwidth rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_bandwidth_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_bandwidth_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMinimumBandwidthRuleDetails(policyId, ruleId, callback)</td>
    <td style="padding:15px">Show minimum bandwidth rule details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_bandwidth_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_bandwidth_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMinimumBandwidthRule(policyId, ruleId, body, callback)</td>
    <td style="padding:15px">Update minimum bandwidth rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_bandwidth_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_bandwidth_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMinimumBandwidthRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Delete minimum bandwidth rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_bandwidth_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_bandwidth_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMinimumPacketRateRulesForQoSPolicy(minKpps, id, direction, sortDir, sortKey, policyId, callback)</td>
    <td style="padding:15px">List minimum packet rate rules for QoS policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_packet_rate_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_packet_rate_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMinimumPacketRateRule(policyId, body, callback)</td>
    <td style="padding:15px">Create minimum packet rate rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_packet_rate_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_packet_rate_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMinimumPacketRateRuleDetails(policyId, ruleId, callback)</td>
    <td style="padding:15px">Show minimum packet rate rule details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_packet_rate_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_packet_rate_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMinimumPacketRateRule(policyId, ruleId, body, callback)</td>
    <td style="padding:15px">Update minimum packet rate rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_packet_rate_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_packet_rate_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMinimumPacketRateRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Delete minimum packet rate rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/minimum_packet_rate_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/minimum_packet_rate_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPacketRateLimitRulesForQoSPolicy(maxKpps, maxBurstKpps, id, direction, sortDir, sortKey, policyId, callback)</td>
    <td style="padding:15px">List packet rate limit rules for QoS policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/packet_rate_limit_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/packet_rate_limit_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPacketRateLimitRule(policyId, body, callback)</td>
    <td style="padding:15px">Create packet rate limit rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/packet_rate_limit_rules?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/packet_rate_limit_rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPacketRateLimitRuleDetails(policyId, ruleId, callback)</td>
    <td style="padding:15px">Show packet rate limit rule details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/packet_rate_limit_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/packet_rate_limit_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePacketRateLimitRule(policyId, ruleId, body, callback)</td>
    <td style="padding:15px">Update packet rate limit rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/packet_rate_limit_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/packet_rate_limit_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePacketRateLimitRule(policyId, ruleId, callback)</td>
    <td style="padding:15px">Delete packet rate limit rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/policies/{pathv1}/packet_rate_limit_rules/{pathv2}?{query} <br /> v1:{base_path}/{version}/qos/policies/{pathv1}/packet_rate_limit_rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBandwidthLimitRuleDetailsAlias(ruleId, callback)</td>
    <td style="padding:15px">Show bandwidth limit rule details alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_bandwidth_limit_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_bandwidth_limit_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBandwidthLimitRuleAlias(ruleId, callback)</td>
    <td style="padding:15px">Update bandwidth limit rule alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_bandwidth_limit_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_bandwidth_limit_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBandwidthLimitRuleAlias(ruleId, callback)</td>
    <td style="padding:15px">Delete bandwidth limit rule alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_bandwidth_limit_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_bandwidth_limit_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDSCPMarkingRuleDetailsAlias(dscpRuleId, callback)</td>
    <td style="padding:15px">Show DSCP marking rule details alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_dscp_marking_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_dscp_marking_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDSCPMarkingRuleAlias(dscpRuleId, callback)</td>
    <td style="padding:15px">Update DSCP marking rule alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_dscp_marking_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_dscp_marking_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDSCPMarkingRuleAlias(dscpRuleId, callback)</td>
    <td style="padding:15px">Delete DSCP marking rule alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_dscp_marking_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_dscp_marking_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMinimumBandwidthRuleDetailsAlias(ruleId, callback)</td>
    <td style="padding:15px">Show minimum bandwidth rule details alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_minimum_bandwidth_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_minimum_bandwidth_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMinimumBandwidthRuleAlias(ruleId, callback)</td>
    <td style="padding:15px">Update minimum bandwidth rule alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_minimum_bandwidth_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_minimum_bandwidth_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMinimumBandwidthRuleAlias(ruleId, callback)</td>
    <td style="padding:15px">Delete minimum bandwidth rule alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_minimum_bandwidth_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_minimum_bandwidth_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMinimumPacketRateRuleDetailsAlias(ruleId, callback)</td>
    <td style="padding:15px">Show minimum packet rate rule details alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_minimum_packet_rate_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_minimum_packet_rate_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMinimumPacketRateRuleAlias(ruleId, callback)</td>
    <td style="padding:15px">Update minimum packet rate rule alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_minimum_packet_rate_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_minimum_packet_rate_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMinimumPacketRateRuleAlias(ruleId, callback)</td>
    <td style="padding:15px">Delete minimum packet rate rule alias</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/qos/alias_minimum_packet_rate_rules/{pathv1}?{query} <br /> v1:{base_path}/{version}/qos/alias_minimum_packet_rate_rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLoggingResources(fields, callback)</td>
    <td style="padding:15px">List Logging Resources</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources?{query} <br /> v1:{base_path}/{version}/logging/logging_resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLoggingResource(body, callback)</td>
    <td style="padding:15px">Create Logging Resource</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources?{query} <br /> v1:{base_path}/{version}/logging/logging_resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showLoggingResourceDetails(fields, loggingResourceId, callback)</td>
    <td style="padding:15px">Show Logging Resource Details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources/{pathv1}?{query} <br /> v1:{base_path}/{version}/logging/logging_resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLoggingResource(loggingResourceId, body, callback)</td>
    <td style="padding:15px">Update Logging Resource</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources/{pathv1}?{query} <br /> v1:{base_path}/{version}/logging/logging_resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLoggingResource(loggingResourceId, callback)</td>
    <td style="padding:15px">Delete Logging Resource</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources/{pathv1}?{query} <br /> v1:{base_path}/{version}/logging/logging_resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFirewallLogs(fields, loggingResourceId, callback)</td>
    <td style="padding:15px">List Firewall Logs</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources/{pathv1}/firewall_logs?{query} <br /> v1:{base_path}/{version}/logging/logging_resources/{pathv1}/firewall_logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFirewallLog(loggingResourceId, body, callback)</td>
    <td style="padding:15px">Create Firewall Log</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources/{pathv1}/firewall_logs?{query} <br /> v1:{base_path}/{version}/logging/logging_resources/{pathv1}/firewall_logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFirewallLogDetails(fields, loggingResourceId, firewallLogId, callback)</td>
    <td style="padding:15px">Show Firewall Log Details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources/{pathv1}/firewall_logs/{pathv2}?{query} <br /> v1:{base_path}/{version}/logging/logging_resources/{pathv1}/firewall_logs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFirewallLog(loggingResourceId, firewallLogId, body, callback)</td>
    <td style="padding:15px">Update Firewall Log</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources/{pathv1}/firewall_logs/{pathv2}?{query} <br /> v1:{base_path}/{version}/logging/logging_resources/{pathv1}/firewall_logs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallLog(loggingResourceId, firewallLogId, callback)</td>
    <td style="padding:15px">Delete Firewall Log</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/logging/logging_resources/{pathv1}/firewall_logs/{pathv2}?{query} <br /> v1:{base_path}/{version}/logging/logging_resources/{pathv1}/firewall_logs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBGPVPNs(fields, callback)</td>
    <td style="padding:15px">List BGP VPNs</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBGPVPNS(body, callback)</td>
    <td style="padding:15px">Create BGP VPNS</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBGPVPNDetails(bgpvpnId, callback)</td>
    <td style="padding:15px">Show BGP VPN details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateABGPVPN(bgpvpnId, body, callback)</td>
    <td style="padding:15px">Update a BGP VPN</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBGPVPN(bgpvpnId, callback)</td>
    <td style="padding:15px">Delete BGP VPN</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkAssociations(fields, bgpvpnId, callback)</td>
    <td style="padding:15px">List Network Associations</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/network_associations?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/network_associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkAssociation(bgpvpnId, body, callback)</td>
    <td style="padding:15px">Create Network Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/network_associations?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/network_associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showNetworkAssociationDetails(bgpvpnId, networkAssociationId, callback)</td>
    <td style="padding:15px">Show Network Association details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/network_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/network_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkAssociation(bgpvpnId, networkAssociationId, callback)</td>
    <td style="padding:15px">Delete Network Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/network_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/network_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRouterAssociations(fields, bgpvpnId, callback)</td>
    <td style="padding:15px">List Router Associations</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/router_associations?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/router_associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouterAssociation(bgpvpnId, body, callback)</td>
    <td style="padding:15px">Create Router Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/router_associations?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/router_associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showRouterAssociationDetails(bgpvpnId, routerAssociationId, callback)</td>
    <td style="padding:15px">Show Router Association details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/router_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/router_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateARouterAssociationBgpvpnRoutesControlExtension(bgpvpnId, routerAssociationId, body, callback)</td>
    <td style="padding:15px">Update a Router Association (‘bgpvpn-routes-control’ extension)</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/router_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/router_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouterAssociation(bgpvpnId, routerAssociationId, callback)</td>
    <td style="padding:15px">Delete Router Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/router_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/router_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPortAssociations(fields, bgpvpnId, callback)</td>
    <td style="padding:15px">List Port Associations</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/port_associations?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/port_associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortAssociation(bgpvpnId, body, callback)</td>
    <td style="padding:15px">Create Port Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/port_associations?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/port_associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPortAssociationDetails(bgpvpnId, portAssociationId, callback)</td>
    <td style="padding:15px">Show Port Association details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/port_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/port_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAPortAssociation(bgpvpnId, portAssociationId, body, callback)</td>
    <td style="padding:15px">Update a Port Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/port_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/port_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortAssociation(bgpvpnId, portAssociationId, callback)</td>
    <td style="padding:15px">Delete Port Association</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgpvpn/bgpvpns/{pathv1}/port_associations/{pathv2}?{query} <br /> v1:{base_path}/{version}/bgpvpn/bgpvpns/{pathv1}/port_associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBGPSpeakers(fields, callback)</td>
    <td style="padding:15px">List BGP Speakers</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers?{query} <br /> v1:{base_path}/{version}/bgp-speakers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBGPSpeaker(body, callback)</td>
    <td style="padding:15px">Create BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers?{query} <br /> v1:{base_path}/{version}/bgp-speakers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBGPSpeakerDetails(bgpSpeakerId, callback)</td>
    <td style="padding:15px">Show BGP Speaker details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgp-speakers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateABGPSpeaker(bgpSpeakerId, body, callback)</td>
    <td style="padding:15px">Update a BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgp-speakers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteABGPSpeaker(bgpSpeakerId, callback)</td>
    <td style="padding:15px">Delete a BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgp-speakers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBGPPeerToABGPSpeaker(bgpSpeakerId, body, callback)</td>
    <td style="padding:15px">Add BGP Peer to a BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/{pathv1}/add-bgp-peer?{query} <br /> v1:{base_path}/{version}/bgp-speakers/{pathv1}/add-bgp-peer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeBGPPeerFromABGPSpeaker(bgpSpeakerId, body, callback)</td>
    <td style="padding:15px">Remove BGP Peer from a BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/{pathv1}/remove-bgp-peer?{query} <br /> v1:{base_path}/{version}/bgp-speakers/{pathv1}/remove-bgp-peer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkToABGPSpeaker(bgpSpeakerId, body, callback)</td>
    <td style="padding:15px">Add Network to a BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/{pathv1}/add_gateway_network?{query} <br /> v1:{base_path}/{version}/bgp-speakers/{pathv1}/add_gateway_network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkFromABGPSpeaker(callback)</td>
    <td style="padding:15px">Delete Network from a BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/remove_gateway_network?{query} <br /> v1:{base_path}/{version}/bgp-speakers/remove_gateway_network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoutesAdvertisedByABGPSpeaker(callback)</td>
    <td style="padding:15px">List routes advertised by a BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/get_advertised_routes?{query} <br /> v1:{base_path}/{version}/bgp-speakers/get_advertised_routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDynamicRoutingAgentsHostingASpecificBGPSpeaker(callback)</td>
    <td style="padding:15px">List Dynamic Routing Agents hosting a specific BGP Speaker</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-speakers/bgp-dragents?{query} <br /> v1:{base_path}/{version}/bgp-speakers/bgp-dragents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBGPPeers(fields, callback)</td>
    <td style="padding:15px">List BGP Peers</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-peers?{query} <br /> v1:{base_path}/{version}/bgp-peers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createABGPPeer(body, callback)</td>
    <td style="padding:15px">Create a BGP Peer</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-peers?{query} <br /> v1:{base_path}/{version}/bgp-peers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBGPPeerDetails(bgpPeerId, callback)</td>
    <td style="padding:15px">Show BGP Peer details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-peers/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgp-peers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateABGPPeer(bgpPeerId, body, callback)</td>
    <td style="padding:15px">Update a BGP Peer</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-peers/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgp-peers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteABGPPeer(bgpPeerId, callback)</td>
    <td style="padding:15px">Delete a BGP Peer</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/bgp-peers/{pathv1}?{query} <br /> v1:{base_path}/{version}/bgp-peers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLogs(id, name, description, tenantId, projectId, event, revisionNumber, resourceType, resourceId, targetId, enabled, sortDir, sortKey, fields, callback)</td>
    <td style="padding:15px">List logs</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/log/logs?{query} <br /> v1:{base_path}/{version}/log/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLog(body, callback)</td>
    <td style="padding:15px">Create log</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/log/logs?{query} <br /> v1:{base_path}/{version}/log/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showLog(logId, callback)</td>
    <td style="padding:15px">Show log</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/log/logs/{pathv1}?{query} <br /> v1:{base_path}/{version}/log/logs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLog(logId, body, callback)</td>
    <td style="padding:15px">Update log</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/log/logs/{pathv1}?{query} <br /> v1:{base_path}/{version}/log/logs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLog(logId, callback)</td>
    <td style="padding:15px">Delete log</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/log/logs/{pathv1}?{query} <br /> v1:{base_path}/{version}/log/logs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLoggableResources(callback)</td>
    <td style="padding:15px">List loggable resources</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/log/loggable-resources?{query} <br /> v1:{base_path}/{version}/log/loggable-resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllAgents(adminStateUp, agentType, alive, availabilityZone, binary, description, host, id, topic, fields, sortDir, sortKey, callback)</td>
    <td style="padding:15px">List all agents</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents?{query} <br /> v1:{base_path}/{version}/agents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAgentDetails(fields, agentId, callback)</td>
    <td style="padding:15px">Show agent details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAgent(agentId, body, callback)</td>
    <td style="padding:15px">Update agent</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAgent(agentId, callback)</td>
    <td style="padding:15px">Delete agent</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllAvailabilityZones(state, resource, name, callback)</td>
    <td style="padding:15px">List all availability zones</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/availability_zones?{query} <br /> v1:{base_path}/{version}/availability_zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoutersHostedByAnL3Agent(agentId, callback)</td>
    <td style="padding:15px">List routers hosted by an L3 agent</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}/l3-routers?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}/l3-routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleRouterToAnL3Agent(agentId, body, callback)</td>
    <td style="padding:15px">Schedule router to an l3 agent</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}/l3-routers?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}/l3-routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeL3RouterFromAnL3Agent(agentId, routerId, callback)</td>
    <td style="padding:15px">Remove l3 router from an l3 agent</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}/l3-routers/{pathv2}?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}/l3-routers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listL3AgentsHostingARouter(routerId, callback)</td>
    <td style="padding:15px">List L3 agents hosting a router</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/routers/{pathv1}/l3-agents?{query} <br /> v1:{base_path}/{version}/routers/{pathv1}/l3-agents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworksHostedByADHCPAgent(agentId, callback)</td>
    <td style="padding:15px">List networks hosted by a DHCP agent</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}/dhcp-networks?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}/dhcp-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleANetworkToADHCPAgent(agentId, body, callback)</td>
    <td style="padding:15px">Schedule a network to a DHCP agent</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}/dhcp-networks?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}/dhcp-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeNetworkFromADHCPAgent(agentId, networkId, callback)</td>
    <td style="padding:15px">Remove network from a DHCP agent</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/agents/{pathv1}/dhcp-networks/{pathv2}?{query} <br /> v1:{base_path}/{version}/agents/{pathv1}/dhcp-networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDHCPAgentsHostingANetwork(networkId, callback)</td>
    <td style="padding:15px">List DHCP agents hosting a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/networks/{pathv1}/dhcp-agents?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/dhcp-agents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAutoAllocatedTopologyDetails(fields, projectId, callback)</td>
    <td style="padding:15px">Show auto allocated topology details</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/auto-allocated-topology/{pathv1}?{query} <br /> v1:{base_path}/{version}/auto-allocated-topology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTheAutoAllocatedTopology(projectId, callback)</td>
    <td style="padding:15px">Delete the auto allocated topology</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/auto-allocated-topology/{pathv1}?{query} <br /> v1:{base_path}/{version}/auto-allocated-topology/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTapServices(project, projectDomain, callback)</td>
    <td style="padding:15px">List Tap Services</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_services?{query} <br /> v1:{base_path}/{version}/taas/tap_services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTapService(project, projectDomain, tenantId, name, port, description, callback)</td>
    <td style="padding:15px">Create Tap Service</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_services?{query} <br /> v1:{base_path}/{version}/taas/tap_services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTapService(name, description, callback)</td>
    <td style="padding:15px">Update Tap Service</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_services/{pathv1}?{query} <br /> v1:{base_path}/{version}/taas/tap_services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTapService(name, id, callback)</td>
    <td style="padding:15px">Delete Tap Service</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_services/{pathv1}?{query} <br /> v1:{base_path}/{version}/taas/tap_services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showTapService(project, projectDomain, name, callback)</td>
    <td style="padding:15px">Show Tap Service</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_services/{pathv1}?{query} <br /> v1:{base_path}/{version}/taas/tap_services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTapFlow(project, projectDomain, callback)</td>
    <td style="padding:15px">List Tap Flow</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_flows?{query} <br /> v1:{base_path}/{version}/taas/tap_flows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTapFlow(project, projectDomain, tenantId, name, port, tapService, vlanFilter, direction, description, callback)</td>
    <td style="padding:15px">Create Tap Flow</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_flows?{query} <br /> v1:{base_path}/{version}/taas/tap_flows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTapFlow(name, description, callback)</td>
    <td style="padding:15px">Update Tap Flow</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_flows/{pathv1}?{query} <br /> v1:{base_path}/{version}/taas/tap_flows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTapFlow(id, name, callback)</td>
    <td style="padding:15px">Delete Tap Flow</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_flows/{pathv1}?{query} <br /> v1:{base_path}/{version}/taas/tap_flows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showTapFlow(project, projectDomain, tenantId, id, name, callback)</td>
    <td style="padding:15px">Show Tap Flow</td>
    <td style="padding:15px">v0:{base_path}/{version}/v2.0/taas/tap_flows/{pathv1}?{query} <br /> v1:{base_path}/{version}/taas/tap_flows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
